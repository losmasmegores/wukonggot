
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;


public class CanvasManager : MonoBehaviour
{
    public TransformationsController transformationsController;
    public Image Image;
    public GameObject UI;
    
    private bool active = false;

    [Header("Sprites Transformations")] public List<Sprite> transformations = new List<Sprite>();

    [Header("Controls")] 
    public GameObject Base;
    public GameObject Frog;
    public GameObject Gorilla;
    public GameObject Squirrel;
    public GameObject V;
    

    void Update()
    {
        if (UI.activeSelf == false)
        {
            Disable();
            active = false;
            V.SetActive(true);
        }
        else
        {
            active = true;
            V.SetActive(false);
        }
        if (transformationsController.nextTransformation == 0)
        {
            if (active)
            {
                Disable();
                Image.sprite = transformations[0];
                Enable(Base);
            }
        }
        else if (transformationsController.nextTransformation == 1)
        {
            if (active)
            {
                Disable();
                Image.sprite = transformations[1];
                Enable(Frog);
            }
        }
        else if (transformationsController.nextTransformation == 2)
        {
            if (active)
            {
                Disable();
                Image.sprite = transformations[2];
                Enable(Gorilla);
            }
        }
        else if (transformationsController.nextTransformation == 3)
        {
            if (active)
            {
                Disable();
                Image.sprite = transformations[3];
                Enable(Squirrel);
            }
        }
        
    }
    
    private void Enable(GameObject go)
    {
        go.SetActive(true);
    }
    private void Disable()
    {
        if (Base.activeSelf)
        {
            Base.SetActive(false);
        }
        else if (Frog.activeSelf)
        {
            Frog.SetActive(false);
        }
        else if (Gorilla.activeSelf)
        {
            Gorilla.SetActive(false);
        }
        else if (Squirrel.activeSelf)
        {
            Squirrel.SetActive(false);
        }
    }
}
