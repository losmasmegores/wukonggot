using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using Interficies;

public class FrogController : MonoBehaviour
{
    [Header("References")]
    public LineRenderer lr;
    public Transform gunTip, cam, player;
    public LayerMask whatIsGrappleable;
    // public PlayerMovementGrappling pm;
    public PlayerMovement mja;

    [Header("Swinging")]
    private float maxSwingDistance = 25f;
    private Vector3 swingPoint;
    private SpringJoint joint;

    [Header("OdmGear")]
    public Transform orientation;
    public Rigidbody rb;
    public float horizontalThrustForce;
    public float forwardThrustForce;
    public float attractForce;
    public float extendCableSpeed;
    public float maxRopeDistance;

    [Header("Prediction")]
    public RaycastHit predictionHit;
    public float predictionSphereCastRadius;
    public Transform predictionPoint;
    
    [Header("Input")] 
    // [SerializeField] private InputActionAsset m_Input;
    [SerializeField] private CharacterInput characterInput; 
    private InputActionAsset m_Input;

    private InputAction m_InputMove;
    private InputAction m_Tongue;
    private InputAction m_goOrAttract;

    private GameObject attractedObject;
    private bool isAttractingObjectOrGoingPlace = false;

    void Awake()
    {
        m_Input = characterInput.m_Input;
        InputActionMap m_map = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Frog));
        m_InputMove =  m_map.FindAction("Movimiento");
        m_Tongue = m_map.FindAction("lengua");
        m_Tongue.started += StartSwing;
        m_Tongue.canceled += StopSwing;
        m_goOrAttract = m_map.FindAction("ir/atraer");
        m_goOrAttract.started += AttractMovableObject;
        m_goOrAttract.canceled += DesAttractMovableObject;
    }

    private void Update()
    {

        CheckForSwingPoints();

        if (joint != null) OdmGearMovement();
    }

    private void AttractMovableObject(InputAction.CallbackContext call)
    {
        if (joint != null)
        {
            isAttractingObjectOrGoingPlace = true;
            // StartCoroutine(atraerIr());
        }
        
    }
    private void DesAttractMovableObject(InputAction.CallbackContext call)
    {
        if (joint != null)
        {
            isAttractingObjectOrGoingPlace = false;
        }
        
    }


    private void LateUpdate()
    {
        DrawRope();
    }

    private void CheckForSwingPoints()
    {
        if (joint != null) return;

        RaycastHit sphereCastHit;
        Physics.SphereCast(cam.position, predictionSphereCastRadius, cam.forward,
                            out sphereCastHit, maxSwingDistance, whatIsGrappleable);

        //IParedRana paredRanaSphereCast = sphereCastHit.collider.gameObject.TryGetComponent<IParedRana>();

        RaycastHit raycastHit;
        Physics.Raycast(cam.position, cam.forward,
                            out raycastHit, maxSwingDistance, whatIsGrappleable);

        Vector3 realHitPoint;

        //IParedRana paredRanaRaycast = raycastHit.collider.gameObject.GetComponent<IParedRana>();

        // Option 1 - Direct Hit
        if (raycastHit.point != Vector3.zero && raycastHit.collider.TryGetComponent<IFrogWall>(out IFrogWall p1))
        {
            
            realHitPoint = raycastHit.point;
        }

        // Option 2 - Indirect (predicted) Hit
        else if (sphereCastHit.point != Vector3.zero && sphereCastHit.collider.TryGetComponent<IFrogWall>(out IFrogWall p2))
        {
            // Debug.Log("IParedRana guay");
            realHitPoint = sphereCastHit.point;

        }

        // Option 3 - Miss
        else
            realHitPoint = Vector3.zero;

        // realHitPoint found
        if (realHitPoint != Vector3.zero)
        {
            predictionPoint.gameObject.SetActive(true);
            predictionPoint.position = realHitPoint;
        }
        // realHitPoint not found
        else
        {
            predictionPoint.gameObject.SetActive(false);
        }

        predictionHit = raycastHit.point == Vector3.zero ? sphereCastHit : raycastHit;
    }


    private void StartSwing(InputAction.CallbackContext call)
    {
        // return if predictionHit not found
        if (predictionHit.point == Vector3.zero || !predictionHit.collider.TryGetComponent<IFrogWall>(out IFrogWall p3)) 
        { 
            Debug.Log("No prediction hit found");
            return; 
        }

        mja.Swinging(true);

        swingPoint = predictionHit.point;
        joint = player.gameObject.AddComponent<SpringJoint>();
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedAnchor = swingPoint;

        float distanceFromPoint = Vector3.Distance(player.position, swingPoint);

        joint.maxDistance = distanceFromPoint * 0.8f;
        joint.minDistance = distanceFromPoint * 0.25f;

        joint.spring = 4.5f;
        joint.damper = 7f;
        // joint.damper = 1f;
        joint.massScale = 4.5f;

        lr.positionCount = 2;
        currentGrapplePosition = gunTip.position;


        IMovable movable = predictionHit.collider.gameObject.GetComponent<IMovable>();

        if (movable != null)
        {
            movable.Move();
            attractedObject = predictionHit.collider.gameObject;
        }

    }

    public void StopSwing(InputAction.CallbackContext call)
    {
        mja.Swinging(false);
        // pm.swinging = false;
        // rb.useGravity = false;
        lr.positionCount = 0;

        Destroy(joint);

        attractedObject = null; // Reset the attracted object
    }

    private void OdmGearMovement()
    {
        if (m_InputMove.ReadValue<Vector2>().y > 0) rb.AddForce(orientation.right * horizontalThrustForce * Time.deltaTime);
        if (m_InputMove.ReadValue<Vector2>().y < 0) rb.AddForce(-orientation.right * horizontalThrustForce * Time.deltaTime);
        
        if (m_InputMove.ReadValue<Vector2>().x > 0) rb.AddForce(orientation.forward * horizontalThrustForce * Time.deltaTime);

        if (isAttractingObjectOrGoingPlace)
        {
            if (attractedObject != null)
            {
                Vector3 directionToPlayer = (player.position - attractedObject.transform.position).normalized;

                float distanceToPlayer = Vector3.Distance(player.position, attractedObject.transform.position);
                
                float attractionForce = Mathf.Lerp(0, forwardThrustForce, 1 - (distanceToPlayer / maxSwingDistance));
                
                attractedObject.GetComponent<Rigidbody>().AddForce(directionToPlayer * attractForce);
            }
            else
            {
                Vector3 directionToPoint = swingPoint - transform.position;
                rb.AddForce(directionToPoint.normalized * forwardThrustForce * Time.deltaTime);
                
                float distanceFromPoint = Vector3.Distance(transform.position, swingPoint);
                
                joint.maxDistance = distanceFromPoint * 0.8f;
                joint.minDistance = distanceFromPoint * 0.25f;
                
            }

        } 
        
        
        
        if (m_InputMove.ReadValue<Vector2>().x < 0)
        {
            float extendedDistanceFromPoint = Vector3.Distance(transform.position, swingPoint) + extendCableSpeed;
            if (Math.Abs(extendedDistanceFromPoint) >= maxRopeDistance) return;
            joint.maxDistance = extendedDistanceFromPoint * 0.8f;
            joint.minDistance = extendedDistanceFromPoint * 0.25f;
        }
    }

    private Vector3 currentGrapplePosition;

    private void DrawRope()
    {
        if (!joint) return;
        if (attractedObject != null)
        {
            swingPoint = attractedObject.transform.position;
        }

        currentGrapplePosition =
            Vector3.Lerp(currentGrapplePosition, swingPoint, Time.deltaTime * 8f);

        lr.SetPosition(0, gunTip.position);
        lr.SetPosition(1, currentGrapplePosition);
    }

    private void OnDestroy()
    {
        m_Tongue.started -= StartSwing;
        m_Tongue.canceled -= StopSwing;
        m_goOrAttract.started -= AttractMovableObject;
        m_goOrAttract.canceled -= DesAttractMovableObject;
    }
}
