using UnityEngine;
using Interficies;
using UnityEngine.Serialization;

public class InteractableGorila : MonoBehaviour, IInteractableGorilla
{
    public bool colisionando;
    public LayerMask layerMask;
    public Material colisionMaterial;
    public Material grabMaterial;
    private Material defaultMaterial;
    
    private Rigidbody rb;
    private Vector3 InitVel;
    private Renderer renderer;

    private void Awake()
    {
        renderer = GetComponent<Renderer>();
        defaultMaterial = renderer.material;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        InitVel = Vector3.zero;
    }

    public void InteractGorilla()
    {
        Debug.Log("Object moved");
    }

    private void Update()
    {
        colisionando = Physics.CheckBox(transform.position, transform.localScale / 2, transform.localRotation, layerMask);
        if (GetComponent<Collider>().gameObject.layer == 10)
        {
            if (!colisionando)
            {
                renderer.material = defaultMaterial;
                renderer.material.color = new Color(defaultMaterial.color.r,defaultMaterial.color.g,defaultMaterial.color.b,0.5f);
            }
        }
    }

    private void LateUpdate()
    {
        if (GetComponent<Collider>().gameObject.layer != 10)
        {
            defaultMaterial = renderer.material;
            defaultMaterial.color = new Color(defaultMaterial.color.r, defaultMaterial.color.g, defaultMaterial.color.b, 1);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        float impactVel = InitVel.magnitude;
        if (impactVel > 5 && collision.gameObject.TryGetComponent(out IBreakable breakable))
        {
            Debug.Log("Velocidad choque: " + impactVel);
            breakable.Break();
        }
    }

    private void FixedUpdate()
    {
        InitVel = rb.velocity;
    }


}
