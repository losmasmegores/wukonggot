using UnityEngine;
using Interficies;
using UnityEngine.Serialization;

public class InteractableSquirrel : MonoBehaviour, IInteractableSquirrel
{
    [SerializeField] private bool hasSquirrel = false;
    public void InteractSquirrel()
    {
        Debug.Log("Squirrel interacted");
    }
    
    public void SetHasSquirrel(bool tieneArdilla)
    {
        hasSquirrel = tieneArdilla;
    }
    public bool GetHasSquirrel()
    {
        return hasSquirrel;
    }
}
