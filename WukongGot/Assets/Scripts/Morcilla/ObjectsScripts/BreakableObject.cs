using UnityEngine;
using Interficies;
using UnityEngine.Serialization;

public class BreakableObject : PSystem, IBreakable
{
    private Material materialColor;
    public Material material => materialColor;
    [SerializeField] private bool VariousMats;
    [SerializeField, Min(0)] private int PositionMatToChange;
    //Game event
    public GameEvent GameEventSound;

    private void Awake()
    {
        if(VariousMats) materialColor = GetComponent<MeshRenderer>().materials[PositionMatToChange];
        else materialColor = GetComponent<MeshRenderer>().material;
    }

    public void Break()
    {
        GameEventSound.Raise();
        Debug.Log("Object broken");
        this.gameObject.SetActive(false);
        Enable();
    }
}
