using UnityEngine;

namespace Interficies
{
    public interface IBreakable
    {
        void Break();
    }

    public interface IClimbable
    {
        void Climb();
    }

    public interface IMovable
    {
        void Move();
    }

    public interface IInteractableSquirrel
    {
        void InteractSquirrel();
    }

    public interface IInteractableGorilla
    {
        void InteractGorilla();
    }

    public interface IBounceableBeam
    {
        void BounceBeam(LineRenderer lineRenderer,Transform transform,float largoLaser);
    }

    public interface IFrogWall
    {
        void Pared();
    }

    public interface IBullseyeBeam
    {
        void BullseyeEnable(Color receivedColor);
    }

}
