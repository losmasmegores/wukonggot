using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    private const string savedLevelKey = "nivelGuardado";

    public static GameManager Instance;

    [SerializeField] private TextMeshProUGUI playText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        if (PlayerPrefs.HasKey(savedLevelKey))
        {
            playText.text = "Continue";
        }
        else
        {
            playText.text = "Play";
        }
    }

    public void GuardarNivel(string nivel)
    {
        PlayerPrefs.SetString(savedLevelKey, nivel);
        PlayerPrefs.Save();
    }

    public void CargarNivelGuardado()
    {
        if (PlayerPrefs.HasKey(savedLevelKey))
        {
            string nivelGuardado = PlayerPrefs.GetString(savedLevelKey);
            SceneManager.LoadScene(nivelGuardado);
        }
        else
        {
            SceneManager.LoadScene("Tutorial Base");
        }
    }
    
}
