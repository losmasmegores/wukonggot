
using Interficies;
using UnityEngine;

public class BullseyePainted : PSystem
{

    private Material myMaterial;

    [SerializeField] private Color colorWanted;
    [SerializeField] private float maxDesv;

    
    [Header("Decorate")]
    [SerializeField] private Material greenGood;
    [SerializeField] private Material redBad;
    [SerializeField] private MeshRenderer showWantedColor;
    [SerializeField] private AudioSource bullseyeSound;

    private void Awake()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
        showWantedColor.material.color = colorWanted;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent<IInteractableSquirrel>(out IInteractableSquirrel interactableSquirrel) || other.gameObject.TryGetComponent<PaintBullet>(out PaintBullet balaPintura))
        {
            other.gameObject.TryGetComponent(out MeshRenderer meshRenderer);
            Color receivedColor = meshRenderer.material.color;
            if (receivedColor == colorWanted)
            {
                bullseyeSound.Play();
                GetComponent<MeshRenderer>().material = greenGood;
                Enable();
            }
            else if (receivedColor.r + maxDesv > colorWanted.r && receivedColor.r - maxDesv < colorWanted.r &&
                    receivedColor.g + maxDesv > colorWanted.g && receivedColor.g - maxDesv < colorWanted.g &&
                    receivedColor.b + maxDesv > colorWanted.b && receivedColor.b - maxDesv < colorWanted.b)
            {
                GetComponent<MeshRenderer>().material = greenGood;
                Enable();
            }
            else
            {
                GetComponent<MeshRenderer>().material = redBad;
                Disable();
            }

        }
    }
}
