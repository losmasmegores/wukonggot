using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour
{
    public float aboveHeight;
    public float belowHeight = -5f;
    public float aboveVel = 6f;
    public float belowVel = 6f;
     
    private void Start()
    {
        aboveHeight = transform.position.y;
    }
    
    public void GoDown()
    {  
        StopAllCoroutines();
        StartCoroutine(MoveDoor(transform.position.y, belowHeight, belowVel));
    }

    public void GoUp()
    {
        StopAllCoroutines();
        StartCoroutine(MoveDoor(transform.position.y, aboveHeight, aboveVel));

    }

    IEnumerator MoveDoor(float initHeight, float finalHeight, float vel)
    {

        float initTime = Time.time;
        float distance = Mathf.Abs(finalHeight - initHeight);
        float duration = distance / vel;

        while (Time.time - initTime < duration)
        {
            float alturaActual = Mathf.Lerp(initHeight, finalHeight, (Time.time - initTime) / duration);
            transform.position = new Vector3(transform.position.x, alturaActual, transform.position.z);
            yield return null;
        }

        transform.position = new Vector3(transform.position.x, finalHeight, transform.position.z);
    }
}
