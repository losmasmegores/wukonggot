using UnityEngine;

public class Lever : PSystem
{
    public BoxCollider leftCollider;

    void OnTriggerEnter(Collider other)
    {
        if (other == leftCollider)
        {
            ActivarPalanca();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == leftCollider)
        {
            
            DesactivarPalanca();
        }
    }

    void ActivarPalanca()
    {
        Debug.Log("lever enabled");
        // activada = true;
        Enable();
    }

    void DesactivarPalanca()
    {
        Debug.Log("lever disabled");
        // activada = false;
        Disable();
    }
}
