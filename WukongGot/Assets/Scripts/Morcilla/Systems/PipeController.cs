using UnityEngine;
using Interficies;

public class PipeController : MonoBehaviour, IBreakable
{
    [SerializeField] private GameObject unbroken;
    [SerializeField] private GameObject broken;
    public void Break()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<IInteractableGorilla>(out IInteractableGorilla iog))
        {
            unbroken.SetActive(false);
            broken.SetActive(true);
        }
    }
}
