using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;

public class PressurePlate : PSystem
{
    public delegate void PlatePressed();
    public event PlatePressed OnPlatePressed;

    public event PlatePressed OnPlateUnpressed;
    private bool once = false;
    [SerializeField] private AudioSource plateSound;
    private void OnTriggerEnter(Collider other)
    {
        plateSound.Play();
    }

    private void OnTriggerStay(Collider other)
    {
        gameObject.GetComponentInParent<MeshRenderer>().material.color = Color.green;
        if (!once)
        {
            if(other.transform.gameObject.layer != 14) Enable();
            once = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponentInParent<MeshRenderer>().material.color = Color.grey;
        if (other.transform.gameObject.layer != 14)
        {
            Disable();
            once = false;
        }
    }
}
