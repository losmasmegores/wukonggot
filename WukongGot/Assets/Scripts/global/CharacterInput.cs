using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterInput : MonoBehaviour
{
    [SerializeField] private InputActionAsset m_InputAsset;
    private InputActionAsset myInput;
    public InputActionAsset m_Input => myInput;
    void Awake()
    {
        myInput = Instantiate(m_InputAsset);
        Debug.Log("New Input: " + myInput.name);
    }
}
