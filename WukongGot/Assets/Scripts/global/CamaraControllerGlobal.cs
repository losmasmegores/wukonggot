using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class CamaraControllerGlobal : MonoBehaviour
{
    public float sens = 100f;

    float RotationX = 0f;
    public bool hasClone;

    public GameObject player;
    public GameObject playerRef;
    public LayerMask layer;
    
    [SerializeField] private CharacterInput characterInput;
    private InputActionAsset m_Input;


    void Start()
    {
        m_Input = characterInput.m_Input;
        m_Input.FindActionMap("Base").FindAction("disparo").performed += MoveClone;
        Cursor.lockState = CursorLockMode.Locked;

    }

    void Update()
    {

        float verticall = Input.GetAxis("Mouse Y") * sens * Time.deltaTime;
        float horizontall = Input.GetAxis("Mouse X") * sens * Time.deltaTime;

        RotationX -= verticall;
        RotationX = Mathf.Clamp(RotationX, -35f, 40f);
        //camara
        transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
        //player
        playerRef.transform.Rotate(Vector3.up, horizontall);
        
    }

    private void MoveClone(InputAction.CallbackContext coc)
    {
        if (hasClone)
        {
            RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit, 20F, layer))
                {
                    if (!player.GetComponent<PJcontrollerGlobal>().getClon().GetComponent<MeshRenderer>().enabled)
                        player.GetComponent<PJcontrollerGlobal>().clonar();
                    player.GetComponent<PJcontrollerGlobal>().getClon().GetComponent<ClonController>()
                        .SetDestination(hit.point);
                }
        }
    }
}
