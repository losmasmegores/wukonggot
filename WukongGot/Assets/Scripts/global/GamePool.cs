using UnityEngine;
using UnityEngine.Assertions;

public class GamePool : MonoBehaviour
{
    protected int size;

    protected int activeMembers;
    void Awake()
    {
        size = transform.childCount;
        GameObject go = null;
        for (int i = 0; i < size; i++)
        {
            go = transform.GetChild(i).gameObject;
            if (go.activeSelf)
            {
                activeMembers++;
            }
        }
    }

    public GameObject GetElement()
    {
        GameObject go = null;
        for (int i = 0; i < size; i++)
        {
            go = transform.GetChild(i).gameObject;
            if (!go.activeSelf)
            {
                activeMembers++;
                return go;
            }
        }
        return null;
    }

    public void ReturnElement(GameObject go)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject selfgo = transform.GetChild(i).gameObject;
            if (selfgo == go)
            {
                activeMembers--;
                go.SetActive(false);
                return;
            }
        }

        Assert.IsFalse(false);
    }

    public int getActiveMembers()
    {
        return activeMembers;
    }
}
