using UnityEngine;
using UnityEngine.InputSystem;

public class PJcontrollerGlobal : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 3f;

    [SerializeField]
    private float m_RotationSpeed = 360f;
    private float m_MouseSensitivity = 1f;
    
    private Rigidbody m_Rigidbody;

    //Camera
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField] private CharacterInput characterInput;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    
    [SerializeField]
    private GameObject clon;
    public Transform clonTransformSpawn;
    
    private void Start()
    {
        m_Input = characterInput.m_Input;
        m_Rigidbody = transform.parent.GetComponent<Rigidbody>();
        m_MovementAction = m_Input.FindActionMap("Base").FindAction("Movimiento");
        m_Input.FindActionMap("Base").FindAction("clon").performed += clonar;
         
    }
    private void clonar(InputAction.CallbackContext icc)
    {
        if (!clon.GetComponent<ClonController>().enabled)
        {
            clon.transform.position = clonTransformSpawn.position;
            clon.GetComponent<MeshRenderer>().enabled = true;
            clon.GetComponent<Collider>().gameObject.layer = 11;
            clon.GetComponent<ClonController>().enabled = true;
        }
        else
        {
            clon.transform.position = clonTransformSpawn.position;
            clon.GetComponent<MeshRenderer>().enabled = false;
            clon.GetComponent<Collider>().gameObject.layer = 9;
            clon.GetComponent<ClonController>().enabled = false;
        }
    }
    public void clonar()
    {
        if (!clon.GetComponent<MeshRenderer>().enabled)
        {
            clon.transform.position = clonTransformSpawn.position;
            clon.GetComponent<MeshRenderer>().enabled = true;
            clon.GetComponent<Collider>().gameObject.layer = 11;
            clon.GetComponent<ClonController>().enabled = true;
        }
        else
        {
            clon.transform.position = clonTransformSpawn.position;
            clon.GetComponent<MeshRenderer>().enabled = false;
            clon.GetComponent<Collider>().gameObject.layer = 9;
            clon.GetComponent<ClonController>().enabled = false;
        }
    }
    
    public GameObject getClon()
    {
        return clon;
    }
    
    
    private void OnDestroy()
    {
        m_Input.FindActionMap("Base").FindAction("clon").performed -= clonar;
    }
}



