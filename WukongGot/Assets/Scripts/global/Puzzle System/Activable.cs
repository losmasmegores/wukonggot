using UnityEngine;
using UnityEngine.Events;

public class Activable : MonoBehaviour
{
    public PSystem[] systems;
    private int enabledSystems = 0;
    
    [Tooltip("Response to invoke when Enabled Event is raised.")]
    public UnityEvent EventosActivados;
    
    [Tooltip("Response to invoke when Disabled Event is raised.")]
    public UnityEvent EventosDesactivados;

    private void Awake()
    {
        foreach (PSystem system in systems)
        {
            system.OnEnable += TryEnable;
            system.OnDiable += TryDisable;
        }
    }

    void TryEnable()
    {
        enabledSystems++;
        if (enabledSystems == systems.Length)
        {
            EventosActivados.Invoke();
        }
        
    }
    void TryDisable()
    {
        if (enabledSystems == 0) return;
        if (enabledSystems == systems.Length)
        {
            EventosDesactivados.Invoke();
        }
        enabledSystems--;
    }
}

    