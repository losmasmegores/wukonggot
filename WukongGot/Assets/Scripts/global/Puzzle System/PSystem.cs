using UnityEngine;

public class PSystem : MonoBehaviour
{
    //PSystem = Puzzle System
    //PEvent = Puzzle Event
    public delegate void PEvent();
    public event PEvent OnEnable;

    public event PEvent OnDiable;
    
    [SerializeField] private bool Activado = false;

    public void Enable()
    {
        if (!Activado)
        {
            Activado = true;
            OnEnable?.Invoke();
        }
    }

    public void Disable()
    {
        if (Activado)
        {
            Activado = false;
            OnDiable?.Invoke();
        }
    }
    
}
