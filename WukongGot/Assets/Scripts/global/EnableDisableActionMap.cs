using UnityEngine;
using UnityEngine.InputSystem;

public class EnableDisableActionMap : MonoBehaviour
{
    [SerializeField] private transformations m_ActionMapName;
    [SerializeField] private CharacterInput characterInput; 
    private InputActionAsset m_Input;

    private void OnEnable()
    {
        m_Input = characterInput.m_Input;
        Debug.Log("EnableDisableActionMap: " + m_ActionMapName + " enabled.");
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(m_ActionMapName)).Enable();
    }

    private void OnDisable()
    {
        m_Input = characterInput.m_Input;
        Debug.Log("EnableDisableActionMap: " + m_ActionMapName + " disabled.");
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(m_ActionMapName)).Disable();
    }

}
