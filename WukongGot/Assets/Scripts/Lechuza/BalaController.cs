using System.Collections;
using UnityEngine;

public class BalaController : MonoBehaviour
{
    private void OnEnable()
    {
        StartCoroutine(TimeToDeath());
    }

   IEnumerator TimeToDeath()
    {
            yield return new WaitForSeconds(2f);
            gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision other)
    {

        if (other.transform.CompareTag("Player"))
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
