using UnityEngine;
using UnityEngine.Events;

public class BeamTurret : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public float laserWidth = 9f;
    public LayerMask layerMask;
    public UnityEvent onHit;
    
    private RaycastHit hit;
    private Ray ray;
    
    void Start()
    {
        lineRenderer.positionCount = 2;
    }
    void Update()
    {
        ray = new(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, laserWidth, layerMask))
        {
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, hit.point);
            if(hit.collider.TryGetComponent(out Interficies.IBounceableBeam rl))
            {
                rl.BounceBeam(lineRenderer,hit.transform,laserWidth);
            }
        }
        else
        {
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, transform.position + transform.forward * laserWidth);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * laserWidth);
        
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(hit.point, 0.23f);
        
    }
}
