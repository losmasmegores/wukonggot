using UnityEngine;
using UnityEngine.Serialization;

public class RotateBeam : MonoBehaviour
{
    public bool collision = false;
    private bool flag = false;
    public GameObject bounceBeam;
    private GameObject beam;
    public float rot;
    private Color color;
    private Color receivedColor = Color.white;
    public void setMaterial(Material m)
    {
        addColorList(m);
    }

    private void Awake()
    {
        color = GetComponent<MeshRenderer>().material.color;
    }

    private void addColorList(Material m)
    {
        if (m.color == receivedColor) return;
        receivedColor = m.color;

    }
    public Color ColorBlend(Color receivedColor, Color myColor)
    {
        if (receivedColor == Color.white) return myColor;
        if(myColor == Color.white) return receivedColor;
        else return new Color(
            (receivedColor.r+myColor.r)/2,(receivedColor.g+myColor.g)/2,(receivedColor.b+myColor.b)/2, 1
        );
    }
    void Update()
    {
        if (collision)
        {
            if (!flag)
            {
                generateBeam();
                Color micolor = GetComponent<MeshRenderer>().material.color;
                transform.GetChild(0).gameObject.GetComponent<LineRenderer>().material.color =
                ColorBlend(receivedColor, micolor);
                flag = true;
            }
            else
            {
                Color myColor = GetComponent<MeshRenderer>().material.color;
                if (receivedColor == myColor || transform.childCount == 0) return;
                transform.GetChild(0).gameObject.GetComponent<LineRenderer>().material.color =
                    ColorBlend(receivedColor, myColor);
            }
        }
        else
        {
            if (transform.childCount> 0)
            {
                deleteChildren();
                receivedColor = Color.white;
                flag = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            rot+=5;
        beam.transform.localEulerAngles = new Vector3(rot, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rot-=5;
            beam.transform.localEulerAngles = new Vector3(rot, 0, 0);
        }
    }
    private void deleteChildren()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
    private void generateBeam()
    {
        if (!flag)
        {
                beam = Instantiate(bounceBeam);
                beam.transform.SetParent(this.transform);
                beam.transform.position = this.transform.position;
                beam.transform.rotation = this.transform.rotation;
        }
    }
}
