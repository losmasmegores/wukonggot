using UnityEngine;
using UnityEngine.InputSystem;

public class GorillaController : MonoBehaviour
{
    private GameObject objet;
    public Material material;
    public Material defaultMaterial;

    public Transform camara;
    [SerializeField] private CharacterInput characterInput;
    private InputActionAsset ias;
    
    [Header("Shooting Variables")]
    [SerializeField] private float force = 10f;
    [SerializeField] private float maxForce;
    [SerializeField] private float minForce;
    
    [Header("Aiming Variables")]
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField,Min(2)] private int nPoints;
    [SerializeField] private float PropMass;
    [SerializeField] private float Time;
    [SerializeField] private bool AutomaticTime;
    private bool Aiming = false;

    private void Awake()
    {
        ias = characterInput.m_Input;
        ias.FindActionMap("Gorila").FindAction("Agarrar").performed += Grab;
        ias.FindActionMap("Gorila").FindAction("Soltar").performed += Drop;
        ias.FindActionMap("Gorila").FindAction("ajustar").performed += Adjust;
    }

    private void FixedUpdate()
    {
        if (Aiming)
        {
            lineRenderer.material = material;
            lineRenderer.positionCount = nPoints;
            Vector3[] posiocionesLineRenderer = CalculatePosition(transform.position,transform.forward * force/PropMass,Time);
            lineRenderer.SetPositions(posiocionesLineRenderer);
        }
        else lineRenderer.positionCount = 0;
    }

    private void Drop (InputAction.CallbackContext obj)
    {
        if (objet != null)
        {
            Debug.Log("No Null");
            if ( transform.childCount == 1 )
            {            
            Debug.Log("Object Droped");
                InteractableGorila a = objet.GetComponent<InteractableGorila>();

                if (!a.colisionando)
                {
                    objet.GetComponent<Renderer>().material = defaultMaterial;
                    objet.GetComponent<Collider>().gameObject.layer = 8;
                    objet.transform.position += camara.transform.forward * 0.4f;
                    objet.transform.SetParent(null);
                    objet.GetComponent<Rigidbody>().isKinematic = false;
                    Aiming = false;
                    force = 1;
                }
            }
        }
    }

    
    private void Grab(InputAction.CallbackContext obj)
    {
        if (objet!= null)
        {
            if (transform.childCount < 1)
            {
                objet.transform.SetParent(gameObject.transform);
                objet.transform.position = gameObject.transform.position;
                objet.GetComponent<Rigidbody>().isKinematic = true;
                objet.GetComponent<Collider>().gameObject.layer = 10;
                defaultMaterial = objet.GetComponent<Renderer>().material;
                objet.GetComponent<Renderer>().material = material;
                PropMass = objet.GetComponent<Rigidbody>().mass; 
                Aiming = true;
            }
            else if (transform.childCount == 1)
            {
                InteractableGorila a = objet.GetComponent<InteractableGorila>();
                if (!a.colisionando)
                {
                    Debug.Log("Lanzar objeto");
                    objet.GetComponent<Renderer>().material = defaultMaterial;
                    objet.GetComponent<Collider>().gameObject.layer = 8;
                    objet.transform.SetParent(null);
                    objet.GetComponent<Rigidbody>().isKinematic = false;
                    objet.GetComponent<Rigidbody>().AddForce(camara.forward * force , ForceMode.Impulse); 
                    Aiming = false;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<InteractableGorila>(out InteractableGorila movableObject))
        {
           objet = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        objet = null;
    }

    private Vector3[] CalculatePosition(Vector3 init, Vector3 vel, float time)
    {
        Vector3[] posiciones = new Vector3[nPoints];
        posiciones[0] = init;
        for (int i = 1; i < nPoints; i++)
        {
            float tiempoActual = time * i;
            Vector3 posSinGravedad = vel * tiempoActual;
            Vector3 funcionGravedad = Vector3.up * -0.5f * Physics.gravity.y * tiempoActual * tiempoActual;
            Vector3 posicionActual = init + posSinGravedad - funcionGravedad;
            posiciones[i] = posicionActual;
        }
        return posiciones;
    }
    private void Adjust(InputAction.CallbackContext obj)
    {
        float a = obj.ReadValue<float>();
        if(a > 0 && force < maxForce)force++;
        else if(a < 0 && force > minForce) force--;
    }

    private void OnDestroy()
    {
        ias.FindActionMap("Gorila").FindAction("Agarrar").performed -= Grab;
        ias.FindActionMap("Gorila").FindAction("Soltar").performed -= Drop;
        ias.FindActionMap("Gorila").FindAction("ajustar").performed -= Adjust;
    }
}