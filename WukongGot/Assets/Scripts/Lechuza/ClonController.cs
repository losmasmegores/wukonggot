using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class ClonController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, WALKING, INTERACTING};
    private SwitchMachineStates m_CurrentState;
    
    private Vector3 m_Destination = Vector3.zero;
    public NavMeshAgent agent;
    public LayerMask layer;
    

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        InitState(SwitchMachineStates.IDLE);
    }

    private void ChangeState(SwitchMachineStates newState)
    {

        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;                  

            case SwitchMachineStates.WALKING:

                

                break;

            case SwitchMachineStates.INTERACTING:


                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALKING:
                break;

            case SwitchMachineStates.INTERACTING:

                break;
            default:
                break;
        }
    }

    public void SetDestination(Vector3 destination)
    {
        if (destination != Vector3.zero)
        {
            m_Destination = destination;
        }
    }

    public Vector3 GetDestination()
    {
        return m_Destination;
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                if (GetDestination()!= Vector3.zero)
                {
                    ChangeState(SwitchMachineStates.WALKING);                    
                }
                break;
            case SwitchMachineStates.WALKING:
                if (Vector3.Distance(this.transform.position, GetDestination()) < 0.1f)
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
                else
                {
                    agent.SetDestination(GetDestination());
                }
                break;
            case SwitchMachineStates.INTERACTING:
                // agent.SetDestination(transform.position);
                if (GetDestination()!= Vector3.zero)
                {
                    ChangeState(SwitchMachineStates.WALKING);                    
                }
                break;


            default:
                break;
        }
    }


    private void OnEnable()
    {
        Debug.Log("OnEnable");
        
        m_Destination = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }
    
    public void setPosicion(Transform pos)
    {
        this.transform.position = pos.position;
    }
}
