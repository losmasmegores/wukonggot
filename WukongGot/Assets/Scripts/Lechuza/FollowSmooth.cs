using UnityEngine;

public class FollowSmooth : MonoBehaviour
{
    public Transform pivot;
    public float speed = 10F;
    public Transform player;

    private void Start()
    {
        this.transform.position = pivot.transform.position;
    }

    private void FixedUpdate()
    {
            Vector3 cpos = pivot.position;
            Vector3 spos = Vector3.Lerp(transform.position, cpos, speed * Time.deltaTime);
            transform.position = spos;
            Vector3 forward = player.position - spos;
            transform.forward = Vector3.Lerp(transform.forward, forward, Time.deltaTime);
            
            transform.LookAt(player);
    }
    
}

