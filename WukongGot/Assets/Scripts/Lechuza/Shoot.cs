using System.Collections;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public PoolManager poolManager;
    private bool shooting;
    public SmoothAim smoothAim;
    private Vector3 dir;
    public float BalaVel = 10;
    [Min(0.1f)]public float ReloadTime;

    public void Start()
    {
        dir = transform.up;
        StartCoroutine(disparo());
    }

    IEnumerator disparo()
    {
        while (true)
        {
            if (smoothAim.getDisparando())
            {
                GameObject bala = poolManager.balarequest();
                if (bala != null)
                {
                    bala.transform.position = transform.position;
                    bala.GetComponent<Rigidbody>().velocity = dir * BalaVel;
                }
            }
            yield return new WaitForSeconds(ReloadTime);
        }
    }

    private void Update()
    {
        dir = transform.up;
    }
}
