using Interficies;
using UnityEngine;

public class BeamController : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Vector3 enterPoint;
    public LayerMask layerMask;
    private Refract reff;
    private RotateBeam rotateBeam;
    private BullseyeBeam bullseyeBeam;
    private RaycastHit hit;
    // private bool once = false;
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetWidth(0.3f, 0.3f);
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            if (hit.transform.gameObject.TryGetComponent<Refract>(out Refract refraccion))
            {
                if (reff != null && reff != refraccion)
                {
                    reff.collide = false;
                    reff = refraccion;
                    Debug.Log(transform.name+": on if");
                        
                }
                else
                {
                    reff = refraccion;
                    refraccion.collide = true;
                    refraccion.setMaterial(lineRenderer.material);
                    refraccion.beamAngle = transform.localEulerAngles.x;
                    desactivarRayoRotacion();
                }
            }
            else
            {
                desactivarRayoRefraccion();
            }
            
            if (hit.transform.gameObject.TryGetComponent<RotateBeam>(out RotateBeam rotarrayo))
            {
                if (rotateBeam!= null && rotateBeam != rotarrayo )
                {
                    rotateBeam.collision = false;
                    rotateBeam = rotarrayo;
                }
                else
                {
                    rotateBeam = rotarrayo;
                    rotarrayo.collision = true;
                    rotarrayo.setMaterial(lineRenderer.material);
                    desactivarRayoRefraccion();
                }
            }
            else
            {
                desactivarRayoRotacion();
            }

            if (hit.transform.gameObject.TryGetComponent<PaintableObject>(out PaintableObject objPint))
            {
                objPint.Paint(lineRenderer.material);
                desactivarRayoRefraccion();
                desactivarRayoRotacion();  

            }

            if(hit.transform.gameObject.TryGetComponent<IBullseyeBeam>(out IBullseyeBeam diana))
            {
                if (bullseyeBeam != null && bullseyeBeam != diana)
                {
                    bullseyeBeam.BullseyeDisable();
                    bullseyeBeam = (BullseyeBeam)diana;
                }
                else
                {
                    bullseyeBeam = (BullseyeBeam)diana;
                    diana.BullseyeEnable(lineRenderer.material.color);
                    desactivarRayoRefraccion();
                    desactivarRayoRotacion();
                }
            }
            else
            {
                desactivarDiana();
            }

            if (hit.transform.CompareTag("Player"))
            {
                desactivarRayoRefraccion();
                desactivarRayoRotacion();
                desactivarDiana();
            }
            

               


            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, hit.point);
        }
        else
        {
           
            desactivarRayoRefraccion();
            desactivarRayoRotacion();
            desactivarDiana();

            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, transform.position + transform.forward * 50);
        }



    }
   
    private void desactivarDiana()
    {
        if (bullseyeBeam)
        {
            bullseyeBeam.BullseyeDisable();
        }
    }
    private void desactivarRayoRefraccion()
    {
        if (reff)
        {
            reff.collide = false;
            reff.beamAngle = 0;
        }
    }
    private void desactivarRayoRotacion()
    {
        if (rotateBeam)
        {
            rotateBeam.collision = false;
        }
    }

    public void SetPuntoDeEntrada(Vector3 punto)
    {
        enterPoint = punto;
    }

    private void OnDestroy()
    {
        desactivarRayoRefraccion();
        desactivarRayoRotacion();
        desactivarDiana();
    }
}
