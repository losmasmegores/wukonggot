using UnityEngine;
using UnityEngine.InputSystem;
using Slider = UnityEngine.UI.Slider;

public class ForceController : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset m_Input;
    private InputAction m_force;

    public Slider slider;
    // private InputActionAsset m_Input;
    private void Awake()
    {
        m_force = m_Input.FindActionMap("move").FindAction("ajustar");
    }

    void OnEnable()
    {
        m_force.performed += Force;
        m_force.Enable();
    }

    private void OnDisable()
    {
        m_force.performed -= Force;
        m_force.Disable();
    }

    public void Force(InputAction.CallbackContext icc)
    {
        float val = m_force.ReadValue<float>();
        
        if (val > 0)
        {
            slider.value++;
        }
        else if (val < 0)
        {
            slider.value--;
        }
        


    }

}
