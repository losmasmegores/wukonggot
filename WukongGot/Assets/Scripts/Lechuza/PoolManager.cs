using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField] private GameObject bala;

    [SerializeField] private int poolsize;
    [SerializeField] private List<GameObject> prefabList;



    private void Awake()
    {
        añadirPool(poolsize);
    }
     
    private void añadirPool(int size) 
    {
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(bala);
            go.SetActive(false);
            prefabList.Add(go);
            go.transform.parent = transform;
        }
    }


    public GameObject balarequest()
    {

        for (int i = 0; i < prefabList.Count; i++)
        {  
            if (prefabList[i].activeSelf == false)
            {
                prefabList[i].SetActive(true);
                return prefabList[i];

            }
        }
        return null;
    }

}