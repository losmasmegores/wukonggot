using System.Collections.Generic;
using UnityEngine;

public class Refract : MonoBehaviour
{

    private enum mats { GLASS, WATER, AIR, NONE };
    [SerializeField]
    private mats m_CurrentState;
    private mats m_PreviousState;
    
    public bool collide = false;
    public float beamAngle;
    
    public List<LineRenderer> beamList = new List<LineRenderer>();
    public List<GameObject> objList = new List<GameObject>();
    public List<Color> colorList = new List<Color>();
    
    
    //COLORS
    private Color receiveColor = Color.white;

    
    private bool flag = false;
    public GameObject beamToRefract;
    [Min(1)]public int nBeams;
    private int nLastBeams;

    private void Start()
    {
        m_PreviousState = mats.NONE;
    }
    
    public void setMaterial(Material m)
    {
        addColorList(m);
    }

    void Update()
    {
        if (collide)
        {
            if (m_PreviousState != m_CurrentState)
            {
                Delete();
                State();
                m_PreviousState = m_CurrentState;
            }
            else if (nLastBeams != nBeams)
            {
                Delete();
                State();
                nLastBeams = nBeams;
            }
            ChangeColor();

        }
        else
        {
            Delete();
            m_PreviousState = mats.NONE;
        }
    }

    private void Delete()
    {
        if (objList.Count > 0)
        {
            beamList.Clear();
            objList.Clear();
            deleteChildren();
            flag = false;
        }
    }
    


    private void State()
    {
        switch (m_CurrentState)
        {
            case mats.GLASS:
                if (collide)
                {
                    generateBeam(1);
                    ChangeColor();
                    flag = true;
                }
                break;

            case mats.WATER:
                if (collide)
                {
                    generateBeam(nBeams);
                    ChangeColor();
                    RotarAnguloRayo();
                    flag = true;

                }
                break;

            case mats.AIR:
                if (collide)
                {
                    generateBeam(3);

                }
                break;
        }
    }

    
    private void RotarAnguloRayo()
    {
        if (!flag)
        {
            float angleXObj = 180f / objList.Count;

            int middlePoint = objList.Count / 2;

            for (int i = 0; i < objList.Count; i++)
            {
                GameObject obj = objList[i];
                LineRenderer lineRenderer = obj.GetComponent<LineRenderer>(); 

                float angle;
                if (objList.Count % 2 == 1) 
                {
                             
                    if (i < middlePoint) 
                    {
                        angle = angleXObj * (i + 1);
                    }
                    else if (i > middlePoint)
                    {
                        angle = -angleXObj * (objList.Count - i);
                    }
                    else 
                    {
                        angle = 0f; 
                    }
                }
                else 
                {
                    if (i < middlePoint) 
                    {
                        angle = angleXObj * (i + 1);
                    }
                    else 
                    {
                        angle = -angleXObj * (objList.Count - i);
                    }
                }

                
                obj.transform.Rotate(Vector3.up * angle); 
                obj.transform.Rotate(Vector3.right * beamAngle); 
                
            }
        }
    }
    
    private void generateBeam(int nBeams)
    {
        if (!flag)
        {
            for (int i = 0; i < nBeams; i++)
            {
               GameObject newBeam = Instantiate(beamToRefract);
               newBeam.transform.SetParent(transform);
               newBeam.transform.position = transform.position;
               newBeam.transform.rotation = transform.rotation;
               
               BeamController bouncedBeam = newBeam.GetComponent<BeamController>();
               LineRenderer lineRenderer = newBeam.GetComponent<LineRenderer>();
               
               objList.Add(newBeam);
               beamList.Add(lineRenderer);
            }
        }
    }
    
    private void deleteChildren()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
    private void ChangeColor()
    {

        foreach (LineRenderer lis in beamList)
        {
            Color myColor = GetComponent<MeshRenderer>().material.color;
            if (receiveColor == myColor || transform.childCount == 0) return;
            lis.material.color =
                ColorBlend(receiveColor, myColor);
        } 
    }
    
    
    public Color ColorBlend(Color colorRecibido, Color colorMio)
    {
        if (colorRecibido == Color.white) return colorMio;
        if(colorMio == Color.white) return colorRecibido;
        return new Color(
            (colorRecibido.r+colorMio.r)/2,(colorRecibido.g+colorMio.g)/2,(colorRecibido.b+colorMio.b)/2, 1
        );
    }

    private void addColorList(Material m)
    {
        if (m.color == receiveColor) return;
        receiveColor = m.color;

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("flor"))
        {
            Material rayo = other.gameObject.GetComponent<MeshRenderer>().material;
            addColorList(rayo);
            ChangeColor();

        }
        else if (other.transform.CompareTag("cristal"))
        {
            nBeams++;
            ChangeColor();
            Destroy(other.gameObject);

        }
    }
}
