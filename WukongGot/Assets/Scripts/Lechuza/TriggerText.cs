using System.Collections;
using UnityEngine;

public class TriggerText : MonoBehaviour
{
    public GameObject texto;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(FiveSeconds());
        }
    }

    IEnumerator FiveSeconds()
    {
        texto.SetActive(true);
        yield return new WaitForSeconds(5);
        texto.SetActive(false);
    }
}