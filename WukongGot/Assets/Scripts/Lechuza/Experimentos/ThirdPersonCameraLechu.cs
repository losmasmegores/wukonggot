using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ThirdPersonCameraLechu : MonoBehaviour
{
    private bool flag = false;
    private Vector3 targetPosition;
    
    
    public float sens = 100f;
    float RotationX = 0f;
    public Transform PositionCamera;
    public Transform PositionCameraAbove;
    public Transform PositionCameraBelow;
    
    public Camera camara;

    public float Above = -35f;
    public float Below = 40f; 
    
    [Header("uncheck if Another MeshRenderer")]
    public bool MyMeshRenderer = true;
    public MeshRenderer ifNotMyMeshRenderer;
    
    void Update()
    {
        if (flag)
        {
            camara.transform.position =  Vector3.Lerp(camara.transform.position, PositionCamera.position, 0.1f);

        }
        float verticall = Input.GetAxis("Mouse Y") * sens * Time.deltaTime;
        float horizontall = Input.GetAxis("Mouse X") * sens * Time.deltaTime;
        
        RotationX -= verticall;
        RotationX = Mathf.Clamp(RotationX, Above,Below);
        
        
        RaycastHit hit;
        RaycastHit hit2;
        Vector3 distancebtw = transform.position - camara.transform.position;
       
        
        
        
        if (RotationX >= Below - Below * 0.30f)
        {
            targetPosition = PositionCameraAbove.position;
        }
        else if (RotationX <= Above - Above * 0.55f)
        {
            targetPosition = PositionCameraBelow.position;
        }
        else
        {
            targetPosition = PositionCamera.position;
        }

        
        if (Physics.Raycast(transform.position, distancebtw*-1, out hit, 2f))
        {
            camara.transform.position = Vector3.Lerp(targetPosition, hit.point + .1f*distancebtw.normalized, 0.5f);
            
        }
        else
        {
            camara.transform.position = Vector3.Lerp(camara.transform.position, targetPosition, 0.2f);
        }
        
        if (Physics.Raycast(transform.position, distancebtw*-1, out hit, 0.8F))
        {
            if(MyMeshRenderer)transform.gameObject.GetComponent<MeshRenderer>().enabled = false;
            else ifNotMyMeshRenderer.enabled = false;
            Debug.DrawRay(transform.position, -distancebtw * hit.distance, Color.magenta);
            
        }
        else
        {
            if(MyMeshRenderer)transform.gameObject.GetComponent<MeshRenderer>().enabled = true;
            else ifNotMyMeshRenderer.enabled = true;
        }

        //camera
        camara.transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
        PositionCamera.transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
    }
    
    private void OnEnable()
    {
        Debug.Log("enable");
        flag = true;
        
    }
}
