using UnityEngine;

public class CameraLechu : MonoBehaviour
{
    private bool flag = false;
    
    
    public float sens = 100f;
    float RotationX = 0f;
    public Transform PositionCamera;
    
    public Camera camara;

    public float Below = -35f;
    public float Above = 20f;
    void Update()
    {
        if (flag)
        {
           camara.transform.position =  Vector3.Lerp(camara.transform.position, PositionCamera.position, 0.1f);

        }
        float verticall = Input.GetAxis("Mouse Y") * sens * Time.deltaTime;
        float horizontall = Input.GetAxis("Mouse X") * sens * Time.deltaTime;
        
        RotationX -= verticall;
        RotationX = Mathf.Clamp(RotationX, Below, Above);
        //camera
        camara.transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
        //player
        PositionCamera.transform.localRotation = Quaternion.Euler(RotationX, 0, 0);
    }

    private void OnEnable()
    {
        flag = true;
    }
}
