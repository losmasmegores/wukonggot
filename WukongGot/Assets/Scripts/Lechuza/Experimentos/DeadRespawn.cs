using UnityEngine;

public class DeadRespawn : MonoBehaviour
{
    public Transform respawn;
    public Transform chekpoint;
    public float distance = 5;
    public bool Beams;
    public Transform refSpawn;

    private void Start()
    {
        refSpawn = respawn;
    }

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, distance))
        {
            if (hit.collider.CompareTag("Player"))
            {
                hit.collider.transform.parent.position = refSpawn.position;
                hit.transform.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
 
        }
        if (Beams)
        {
            LineRenderer a = gameObject.GetComponent<LineRenderer>();
            a.SetWidth(0.1f, 0.1f);
            a.SetPosition(0, transform.position);
            a.SetPosition(1, hit.point);
        }

    }

    public void changeTransform()
    {
        Debug.Log("change");
        refSpawn = chekpoint;
    }

    public void volverTransofrm()
    {
        refSpawn = respawn;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Beams)
        {
            if (other.transform.parent.TryGetComponent<Rigidbody>(out Rigidbody rb))
            {
                other.transform.parent.position = refSpawn.position;
                rb.velocity = Vector3.zero;
                other.transform.parent.gameObject.GetComponent<PlayerMovement>().Swinging(false);
            }
        }
    }
}