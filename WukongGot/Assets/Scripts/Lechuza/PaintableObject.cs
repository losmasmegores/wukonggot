using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PaintableObject : MonoBehaviour
{
    public List<Color> colorList = new List<Color>();
    private bool flag = false;

    public void Paint(Material material)
    {
        addColorList(material);
        ChangeColor();
    }
    
    private void ChangeColor()
    {
        List<Color> colors = new List<Color>(colorList); 

        Color resultColor = ColorBlend(colors.ToArray());

        gameObject.GetComponent<MeshRenderer>().material.color = resultColor;
    }
    
    public Color ColorBlend(params Color[] colors)
    {
        if (colors == null || colors.Length == 0)
        {
            return Color.white; 
        }

        int nColors = colors.Length;
        float r = 0, g = 0, b = 0;

        foreach (Color color in colors)
        {
            r += color.r;
            g += color.g;
            b += color.b;
        }

        r /= nColors;
        g /= nColors;
        b /= nColors;
        
        r = Mathf.Clamp01(r);
        g = Mathf.Clamp01(g);
        b = Mathf.Clamp01(b);

        return new Color(r, g, b);
    }
    
    private void addColorList(Material m)
    {
        if (!colorList.Contains(m.color))
        {
            colorList.Add(m.color);
        }

    }
}
