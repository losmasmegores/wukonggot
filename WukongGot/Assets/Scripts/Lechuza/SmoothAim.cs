using UnityEngine;

public class SmoothAim : MonoBehaviour
{
    public Transform target;
    public float smoothRotation = 0.125f;
    public float areaDeteccion = 10f;
    public float maxDistance;
    public bool aiming;
    public bool shooting;
    public LayerMask detectLayer;
    public LayerMask shootLayer;


    private void FixedUpdate()
    {
        if (Physics.SphereCast(transform.position, areaDeteccion, transform.forward, out RaycastHit hit, maxDistance, detectLayer))
        {
            Debug.Log("SphereCast " +hit.transform.name+ " Posiciones "+transform.position);
            target = hit.transform;
            aiming = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position+ (transform.forward*maxDistance), areaDeteccion);
    }

    void Update()
    {
        if (aiming)
        {
                Vector3 dir = target.position - transform.position;
                Debug.DrawLine(transform.position, dir * maxDistance, Color.red, 3);
                if(Physics.Raycast(transform.position, dir, out RaycastHit hit, maxDistance, shootLayer))
                {
                    Debug.Log("Raycast " +hit.transform.name);
                    aiming = true;
                    if (hit.transform.gameObject.layer != 6)
                    {
                        shooting = true;
                        target = hit.transform;
                    }
                    else shooting = false;
                }

                if (shooting)
                {
                    Quaternion rot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir.normalized),smoothRotation* Time.deltaTime );
                    rot.z = 0;
                    rot.x = 0;
                    transform.rotation = rot;  
                }
                
             
        }
    }
    
    public bool getAupuntando()
    {
        return aiming;
    }
    public bool getDisparando()
    {
        return shooting;
    }
}
