using UnityEngine;

public class BreakWall : MonoBehaviour
{
    public LayerMask layer;
    public Material material;
    [SerializeField] private float maximaDesv;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, 5f,layer))
            {
                if (hit.transform.gameObject.GetComponent<BreakableObject>().material.color == null && !hit.transform.gameObject.TryGetComponent<PaintAnObject>(out PaintAnObject pd)) return;
                Debug.Log("My color: "+material.color+" Wall color: "+hit.transform.gameObject.GetComponent<BreakableObject>().material.color);
                hit.transform.gameObject.TryGetComponent(out BreakableObject breakableObject);
                Color receivedColor = breakableObject.material.color;
                if (hit.transform.gameObject.TryGetComponent<PaintAnObject>(out PaintAnObject pd2))
                {
                    receivedColor = pd2.myColor;   
                }
                if (receivedColor == material.color)
                {
                    Debug.Log("wall broken");
                    hit.transform.gameObject.GetComponent<BreakableObject>().Break();
                }
                else if (receivedColor.r + maximaDesv > material.color.r &&
                         receivedColor.r - maximaDesv < material.color.r &&
                         receivedColor.g + maximaDesv > material.color.g &&
                         receivedColor.g - maximaDesv < material.color.g &&
                         receivedColor.b + maximaDesv > material.color.b &&
                         receivedColor.b - maximaDesv < material.color.b)
                {
                    Debug.Log("wall broken");
                    hit.transform.gameObject.GetComponent<BreakableObject>().Break();
                }
            }
            Debug.Log("broken?");
        }
    }
}
