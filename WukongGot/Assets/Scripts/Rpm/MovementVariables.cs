using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MovementVariables : MonoBehaviour
{
    [FormerlySerializedAs("velocidad")]
    [Header("Velocity")]
    [SerializeField]private float vel;
    [SerializeField]private float airVel;
    [SerializeField] private float hoveringVel;
    
    [Header("Jumping")]
    [SerializeField] private float jumpForce;
    [SerializeField]private float distanceFromFloor;

    public float Vel
    {
        get => vel;
        set => vel = value;
    }
    public float AirVel
    {
        get => airVel;
        set => airVel = value;
    }
    public float HoveringVel
    {
        get => hoveringVel;
        set => hoveringVel = value;
    }

    public float DistanceToJump => distanceFromFloor;
    public float JumpForce => jumpForce;
}
