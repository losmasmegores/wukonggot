using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class LevelInMenu : MonoBehaviour
{
    [SerializeField] private MenuLevel[] niveles;
    [SerializeField] private GameObject levelPrefab;
    [SerializeField] private GameObject SelectLevelsGameObject;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private LeaderBoardController leaderBoardController;
    [SerializeField] private RectTransform login;
    [SerializeField] private Toggle toogleController;
    [SerializeField] private ScriptableChallengerMode scm;

    private RectTransform rt;
    private Vector3 initialPosition;
    void Awake()
    {
        GameObject go = null;
        int num = 0;
        foreach (MenuLevel lev in niveles)
        {
            num++;
            go = Instantiate(levelPrefab);
            LevelUI levelUI = go.GetComponent<LevelUI>();
            levelUI.SetLevel(lev);
            go.transform.parent = transform;
            go.SetActive(true);
            leaderBoardController.createLevel(num, lev.LevelNameToShow, lev.LevelSceneName);
        }
        rt = GetComponent<RectTransform>();
        RectTransform rtlastGo = go.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, rtlastGo.sizeDelta.y * (niveles.Length-1));
        rt.position = new Vector3(rt.position.x, rt.position.y - rt.sizeDelta.y / 2, rt.position.z);
        initialPosition = rt.position;
        SelectLevelsGameObject.SetActive(false);
    }

    public void setInicialPosition()
    {
        rt.position = initialPosition;
    }

    public void setChallengerMode(bool challengerMode)
    {
        if (!playerController.isLogedIn)
        {
            login.gameObject.SetActive(true);
            toogleController.isOn = false;
        }
        else
        {
            Player player = playerController.getActualPlayer();
            foreach (Transform child in transform)
            {
                LevelUI levelUI = child.GetComponent<LevelUI>();
                levelUI.setChallengerMode(challengerMode);
                levelUI.getScore(player);
            }

            scm.player = player;
            scm.challengeMode = challengerMode;
            scm.playerjsonPath = playerController.playerPath;
            scm.allplayersjsonPath = playerController.playersPath;
        }
    }
}

[Serializable]
public class MenuLevel
{
    [SerializeField] private Sprite Imagen;
    [SerializeField,Tooltip("levelNameToShow")] private string lNTS;
    [SerializeField, Tooltip("levelSceneName")] private string lSN;
    [SerializeField] private string description;
    [FormerlySerializedAs("dificultad")] [SerializeField] private Difficulties difficulty;
    private float time;
    
    public Sprite Image => Imagen;
    public string LevelNameToShow => lNTS;
    public string LevelSceneName => lSN;
    public string Description => description;
    public Difficulties Difficulty => difficulty;
    public float Time => time;
    
}


