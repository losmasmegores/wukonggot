using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MenuBackground : MonoBehaviour
{
    [SerializeField] private Sprite[] Images;
    [SerializeField] private AnimationClip[] animations;
    [SerializeField] private float timeToWait;

    private Sprite lastImage;

    [SerializeField] private GamePool poolImages;
    void Start()
    {
        lastImage = null;
        StartCoroutine(ShowImages());
    }

    IEnumerator ShowImages()
    {
        while (true)
        {
            GameObject go = poolImages.GetElement();
            go.GetComponent<Image>().sprite = Images[Random.Range(0, Images.Length)];
            while(lastImage != null && go.GetComponent<Image>().sprite == lastImage)
            {
                go.GetComponent<Image>().sprite = Images[Random.Range(0, Images.Length)];
            }
            lastImage = go.GetComponent<Image>().sprite;
            go.SetActive(true);
            go.GetComponent<Animator>().Play(animations[Random.Range(0, animations.Length)].name);
            yield return new WaitForSeconds(timeToWait);
        }
    }
}
