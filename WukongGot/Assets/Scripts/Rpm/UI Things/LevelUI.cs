using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class LevelUI : MonoBehaviour
{
    [SerializeField] private Image levelImage;
    [SerializeField] private TextMeshProUGUI levelName;
    [SerializeField] private TextMeshProUGUI Description;
    [SerializeField] private setDifficulty Difficulty;
    [SerializeField] private TextMeshProUGUI Time;
    private String levelSceneName;
    public void SetLevel(MenuLevel l)
    {
        levelImage.sprite = l.Image;
        levelName.text = l.LevelNameToShow;
        levelSceneName = l.LevelSceneName;
        Description.text = l.Description;
        Difficulty.setDifficult(l.Difficulty);
        Time.text = "Tiempo: No jugado";
    }
    
    public void LoadLevel()
    {
        if (File.Exists(Application.persistentDataPath + "/" + "NivelActual.json"))
        {
            string jsonplayers = File.ReadAllText(Application.persistentDataPath + "/" + "NivelActual.json");
            LevelJson level = JsonUtility.FromJson<LevelJson>(jsonplayers);
            if(level.SceneName == levelSceneName)File.Delete(Application.persistentDataPath + "/" + "NivelActual.json");
        }
        SceneManager.LoadScene(levelSceneName);
    }

    public void setChallengerMode(bool ChallengerMode)
    {
        if (ChallengerMode)
        {
            Description.gameObject.SetActive(false);
            Time.gameObject.SetActive(true);
        }
        else
        {
            Time.gameObject.SetActive(false); 
            Description.gameObject.SetActive(true);
        }
    }

    public void getScore(Player p)
    {
        float score = p.GetScore(levelSceneName);
        if(float.IsNaN(score))Time.text = "Tiempo: No jugado";
        else
        {
            int seconds = (int)score / 100;
            int miliseconds = (int)score % 100;
            Time.text = "Tiempo: "+string.Format("{0:00}:{1:00}",seconds,miliseconds);
        }
    }
}
