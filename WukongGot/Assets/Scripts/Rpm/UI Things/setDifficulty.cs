using System;
using TMPro;
using UnityEngine;

public class setDifficulty : MonoBehaviour
{
    [SerializeField] private setDifficulties[] difficulties;
    [SerializeField]private TextMeshProUGUI text;
    public void setDifficult(Difficulties dificultad)
    {
        foreach (setDifficulties d in difficulties)
        {
            if (d.Difficulty == dificultad)
            {
                text.text = dificultad.ToString();
                // text.text = "si";
                text.color = d.Color;
            }
        }
    }
    
}
[Serializable]
public class setDifficulties
{
    [SerializeField] private Difficulties difficulty;
    [SerializeField] private Color color;
    public Difficulties Difficulty => difficulty;
    public Color Color => color;
}
public enum Difficulties
{
    Easy,
    Normal,
    Hard
}
