using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintBullet : MonoBehaviour
{
    [SerializeField] private GameObject quad;
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 6)
        {
            if (GetComponent<MeshRenderer>().material.color.a < 0.5)
            {
                gameObject.SetActive(false);
                return;
            }
            GameObject go = Instantiate(quad);
            go.transform.position = transform.position;
            go.transform.forward = other.GetContact(0).normal * -1;
            go.transform.localScale = transform.localScale;
            if(Physics.Raycast(go.transform.position, go.transform.forward, out RaycastHit hit))
            {
                go.transform.position = hit.point;
                go.transform.position += go.transform.forward * -0.1f;
            }
            go.GetComponent<MeshRenderer>().material.color = GetComponent<MeshRenderer>().material.color;
            go.SetActive(true);
            gameObject.SetActive(false);
        }
        else if(other.gameObject.TryGetComponent<PaintedQuad>(out PaintedQuad quadPintado))
        {
            quadPintado.Paint(GetComponent<MeshRenderer>().material.color);
            gameObject.SetActive(false);
        }
        else gameObject.SetActive(false);
    }
}
