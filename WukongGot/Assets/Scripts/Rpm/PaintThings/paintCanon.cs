using UnityEngine;

public class cañonpintura : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float force;
    [SerializeField] private Color color;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            bullet.GetComponent<MeshRenderer>().material.color = color;
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            bullet.transform.position = transform.position+transform.forward*2;
            bullet.SetActive(true);
            rb.AddForce(transform.forward * force, ForceMode.Impulse);
        }
    }
}
