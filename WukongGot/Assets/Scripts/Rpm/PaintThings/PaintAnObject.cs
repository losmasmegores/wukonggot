using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PaintAnObject : MonoBehaviour
{
    [SerializeField] private Color color;
    [SerializeField] private bool VariousMats;
    [SerializeField, Min(0)] private int matPositionToChangeColor;

    public Color myColor => color;

    private void Awake()
    {
        if(VariousMats) GetComponent<MeshRenderer>().materials[matPositionToChangeColor].color = color;
        else GetComponent<MeshRenderer>().material.color = color;
    }
    
    public void PaintMe(string color)
    {
        Color colorin = getColorfromString(color);
        if(VariousMats) GetComponent<MeshRenderer>().materials[matPositionToChangeColor].color = colorin;
        else GetComponent<MeshRenderer>().material.color = colorin;
    }

    private Color getColorfromString(string c)
    {
        if (c == "Azul" || c == "Blue") return Color.blue;
        else return Color.white;
    }

    public void setColor(string colore)
    {
        Color colorin = getColorfromString(colore);
        color = colorin;
    }
}

