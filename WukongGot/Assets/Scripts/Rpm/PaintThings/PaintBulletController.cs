using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PaintBulletController : MonoBehaviour
{
    [SerializeField] private GameObject paintBullet;
    [SerializeField] private GameObject paintedQuad;

    public GameObject bullet => paintBullet;
    public GameObject quad => paintedQuad;
}
