using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class PaintGenerator : MonoBehaviour
{
    [SerializeField] private GamePool bulletPool;
    [SerializeField] private float waitingTime;
    [SerializeField] private Color color;
    [SerializeField] private float sizeMin;
    [SerializeField] private float sizeMax;
    [SerializeField] private float forceMin;
    [SerializeField] private float forceMax;
    void OnEnable()
    {
        StartCoroutine(StartPainting());
    }

    IEnumerator StartPainting()
    {
        while (true)
        {
            GameObject go = bulletPool.GetElement();
            if (go == null) yield return new WaitForSeconds(waitingTime);
            go.transform.position = transform.position;
            go.transform.forward = transform.forward + 
                                   transform.right * Random.Range(-1, 0.5f) +
                                   transform.up * Random.Range(-1, 0.5f);
            float scale = Random.Range(sizeMin, sizeMax);
            go.transform.localScale = new Vector3(scale, scale, scale);
            go.GetComponent<MeshRenderer>().material.color = new Color(color.r,color.g,color.b,color.a);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            go.SetActive(true);
            rb.AddForce(go.transform.forward * Random.Range(forceMin,forceMax), ForceMode.Impulse);
            yield return new WaitForSeconds(waitingTime);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
