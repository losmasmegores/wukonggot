using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Serialization;

public class PaintedQuad : MonoBehaviour
{
    private Color original;

    [SerializeField] private bool deleteOnClean;
    // Start is called before the first frame update
    private void Awake()
    {
        original = GetComponent<MeshRenderer>().material.color;
    }

    public void Paint(Color color)
    {
        if (color.a < 0.5)
        {
            if(deleteOnClean) Destroy(gameObject);
            else GetComponent<MeshRenderer>().material.color = original;
            return;
        }
        Color micolor = GetComponent<MeshRenderer>().material.color;
        if (micolor == color) return;
        Color colore = new Color((micolor.r + color.r)/2, (micolor.g + color.g)/2, (micolor.b + color.b)/2, 1);
        GetComponent<MeshRenderer>().material.color = colore;
    }
}
