using UnityEngine;
using UnityEngine.InputSystem;
public class TransformationsController : MonoBehaviour
{
    //-------------------INPUT VARIABLES-------------------
    [SerializeField] private CharacterInput characterInput;
    private InputActionAsset m_Input;
    private InputAction m_Transform;
    
    [SerializeField] private int ActualTransformation;
    private PlayerMovement m_PlayerMov;
    public int nextTransformation = 0;
    public bool ShowTransformGUI = true;

    [SerializeField] private Transform transformUI;
    
    [Header("Pause Menu")]
    [SerializeField] private RectTransform pauseMenu;
    private bool isPaused = false;


    public void Start()
    {
        m_Input = characterInput.m_Input;
        m_PlayerMov = GetComponent<PlayerMovement>();
        //-------------------------------INPUT ACTIONS--------------------------------
        //-------------------BASE-------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Base)).FindAction("Transformarse");
        m_Transform.canceled += Transform;
        m_Transform.started += ShowTransformations;
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Base)).FindAction("Transformarse").Enable();
        //------------------------------------------
        //-------------------RANA-------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Frog)).FindAction("Transformarse");
        m_Transform.canceled += Transform;
        m_Transform.started += ShowTransformations;
        //------------------------------------------
        //------------------GORILA------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Gorilla)).FindAction("Transformarse");
        m_Transform.canceled += Transform;
        m_Transform.started += ShowTransformations;
        //------------------------------------------
        //-----------------ARDILLA------------------ 
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Squirrel)).FindAction("Transformarse");
        m_Transform.canceled += Transform;
        m_Transform.started += ShowTransformations;
        //------------------------------------------
        
        m_Input.FindActionMap("GUI").FindAction("Pause").performed += PauseMenu;
        m_Input.FindActionMap("GUI").Enable();
        //----------------------------------------------------------------------------
        Transform();
    }
    

    public void SelectTransformation()
    {
        setTransformControls(nextTransformation);
    }

    public void ShowTransformations(InputAction.CallbackContext call)
    {
        if (!ShowTransformGUI) return;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        transformUI.gameObject.SetActive(true);
    }
    public void Transform(InputAction.CallbackContext call)
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        transform.GetChild(ActualTransformation).gameObject.SetActive(false);
        SelectTransformation();
        transform.GetChild(ActualTransformation).gameObject.SetActive(true);
        transformUI.gameObject.SetActive(false);
    }
    public void Transform()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        transform.GetChild(ActualTransformation).gameObject.SetActive(false);
        SelectTransformation();
        transform.GetChild(ActualTransformation).gameObject.SetActive(true);
        transformUI.gameObject.SetActive(false);
    }


    private void setTransformControls(int i)
    {
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(Transformations.getTransformacionEnumFromInt(ActualTransformation))).FindAction("Transformarse").Disable();
        ActualTransformation = i;
        m_PlayerMov.TransformationVariables(Transformations.getTransformacionEnumFromInt(ActualTransformation));
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(Transformations.getTransformacionEnumFromInt(ActualTransformation))).FindAction("Transformarse").Enable();
    }

    private void OnDestroy()
    {
        m_PlayerMov = GetComponent<PlayerMovement>();
        //-------------------------------INPUT ACTIONS--------------------------------
        //-------------------BASE-------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Base)).FindAction("Transformarse");
        m_Transform.started -= ShowTransformations;
        m_Transform.canceled -= Transform;
        //------------------------------------------
        //-------------------RANA-------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Frog)).FindAction("Transformarse");
        //------------------------------------------
        //------------------GORILA------------------
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Gorilla)).FindAction("Transformarse");
        //------------------------------------------
        //-----------------ARDILLA------------------ 
        m_Transform = m_Input.FindActionMap(Transformations.getTransformacionFromEnum(transformations.Squirrel)).FindAction("Transformarse");
        //------------------------------------------
        m_Input.FindActionMap("GUI").FindAction("Pause").performed -= PauseMenu;
        m_Input.FindActionMap("GUI").Disable();
    }

    public void PauseMenu(InputAction.CallbackContext call)
    {
        PauseMenu();
    }
    public void PauseMenu()
    {
        if (isPaused)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            pauseMenu.gameObject.SetActive(false);
            EnableControls();
            isPaused = false;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            pauseMenu.gameObject.SetActive(true);
            DisableControls();
            isPaused = true;
        }
    }
    private void DisableControls()
    {
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(Transformations.getTransformacionEnumFromInt(ActualTransformation))).Disable();
    }

    private void EnableControls()
    {
        m_Input.FindActionMap(Transformations.getTransformacionFromEnum(Transformations.getTransformacionEnumFromInt(ActualTransformation))).Enable();
    }
}
