using System.Collections.Generic;
using Interficies;
using UnityEngine;

public class SquirrelController : MonoBehaviour
{
    [SerializeField]private Transform player;
    [SerializeField] private List<GameObject> objs = new List<GameObject>();

    public Material climbMaterial;


    public int listSize => objs.Count;
    private Vector3 localUp;

    private void Awake()
    {
        localUp = transform.up;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if((other.gameObject.TryGetComponent<IInteractableSquirrel>(out IInteractableSquirrel squirrel)))
        {
            consume(other.gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {

        if(other.gameObject.TryGetComponent<MeshRenderer>(out MeshRenderer mesh))
        {
            if (mesh.material.color == climbMaterial.color)
            {
                player.TryGetComponent<PlayerMovement>(out PlayerMovement mov);
                mov.ChangeState(PlayerStates.Climbing); 
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        player.TryGetComponent<PlayerMovement>(out PlayerMovement mov);
        mov.ChangeState(PlayerStates.Static);
    }

    public GameObject getGameObject()
    {
        if (objs.Count > 0)
        {
            GameObject go = objs[0];
            objs.RemoveAt(0);
            return go;
        }
        else return null;
    }
    public GameObject getGameObject(int pos)
    {
        if (objs.Count > 0 && pos < objs.Count)
        {
            GameObject go = objs[pos];
            objs.RemoveAt(pos);
            return go;
        }
        else return null;
    }
    public GameObject showGameObject(int pos)
    {
        if (objs.Count > 0 && pos < objs.Count)
        {
            GameObject go = objs[pos];
            return go;
        }
        else return null;
    }
    public void addGameObject(GameObject go)
    {
        objs.Add(go);
        if (go.transform.parent != null &&
            go.transform.parent.TryGetComponent<GamePoolSquirrel>(out GamePoolSquirrel poolBala))
        {
            poolBala.ReturnElementSquirrel(go);
        }
        else go.SetActive(false);
        if(go.TryGetComponent(out InteractableSquirrel squirrel))
        {
            squirrel.SetHasSquirrel(true);
        }
    }

    public bool HasGameObjects()
    {
        return objs.Count > 0;
    }

    public void consume(GameObject other)
    {
        if (other.transform.parent != null &&
            other.transform.parent.TryGetComponent<GamePoolSquirrel>(out GamePoolSquirrel poolBala))
        {
            poolBala.ReturnElementSquirrel(other);
        }
        else other.SetActive(false);
        if(other.TryGetComponent(out InteractableSquirrel squirrel))
        {
            squirrel.SetHasSquirrel(true);
        }
        objs.Add(other);
    }
}
