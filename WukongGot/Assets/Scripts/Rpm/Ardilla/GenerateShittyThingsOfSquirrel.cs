using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class GenerateShittyThingsOfSquirrel : MonoBehaviour
{
    [SerializeField]private GamePoolSquirrel pool;
    [SerializeField] private int maxNuts = 3;
    [SerializeField] private float timeBtwNuts = 5;
    [SerializeField] private Transform spawnPointMin;
    [SerializeField] private Transform spawnPointMax;

    void Start()
    {
        StartCoroutine(GenerateNuts());
    }
    IEnumerator GenerateNuts()
    {
        while (true)
        {
            Debug.Log("Active Nuts : "+pool.getActiveMembers());
            if (pool.getActiveMembers() < maxNuts)
            {
                GameObject nuez = pool.GetElementSquirrel();
                nuez.transform.position = new Vector3(
                    Random.Range(spawnPointMin.position.x, spawnPointMax.position.x),
                    Random.Range(spawnPointMin.position.y, spawnPointMax.position.y),
                    Random.Range(spawnPointMin.position.z, spawnPointMax.position.z)
                    );
                nuez.SetActive(true);
            }
            yield return new WaitForSeconds(timeBtwNuts);
        }
    }
}
