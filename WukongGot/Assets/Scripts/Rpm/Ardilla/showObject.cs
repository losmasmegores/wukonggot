using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class showObject : MonoBehaviour
{
    [SerializeField] private Sprite NoObjImage;
    private Sprite OriginalImage;
    private Image myImage;

    private GameObject lastGameObject = null;


    private void Awake()
    {
        OriginalImage = GetComponent<Image>().sprite;
        myImage = GetComponent<Image>();
    }

    public void showActualObject(GameObject go)
    {
        if (go == null)
        {
            myImage.sprite = NoObjImage;
            myImage.color = Color.white;
        }
        else
        {
            if (go == lastGameObject) return;
            myImage.sprite = OriginalImage;
            if (go.GetComponent<MeshRenderer>().material.color == null) myImage.color = Color.white;
            else myImage.color = go.GetComponent<MeshRenderer>().material.color;
            lastGameObject = go;

        }
    }
}
