
using UnityEngine;
using UnityEngine.InputSystem;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class SquirrelShoot : MonoBehaviour
{
    
    [Header("Materials")]
    [SerializeField] private Material hasBullets;
    [SerializeField] private Material noBullets;
    [SerializeField] private PaintBulletController BulletExample;
    [SerializeField] private LayerMask layerPaint;
    [SerializeField] private int SnifDistance;
    
    [Header("Assets")]
    [SerializeField] private CharacterInput characterInput; 
    private InputActionAsset m_Input;
    [SerializeField] private SquirrelController squirrelController;
    [SerializeField] private showObject GuiActualObj;

    [SerializeField] private Camera camera;
    [SerializeField] private Transform squirrelPosition; 
    [SerializeField] private Transform finalPos;
    [SerializeField] private LayerMask layerMask;
    
    [Header("Shoot variables")]
    [SerializeField] private float force;
    [SerializeField] private float maxForce;
    [SerializeField] private float minForce;
    
    [Header("Aiming")]
    [SerializeField] private LineRenderer lineRenderer;
    [SerializeField,Min(2)] private int nPoints;
    [SerializeField] private float pMass;
    [SerializeField] private float time;
    [SerializeField] private bool automaticTime;



    private int selectedObj = 0;
    private bool aiming = false;
    private Vector3 posFinalFloor;
    void Awake()
    {
        m_Input = characterInput.m_Input;
        m_Input.FindActionMap("Ardilla").FindAction("Disparar").performed += Shoot;
        m_Input.FindActionMap("Ardilla").FindAction("Apuntar").started += YesAim;
        m_Input.FindActionMap("Ardilla").FindAction("Apuntar").canceled += NoAim;
        m_Input.FindActionMap("Ardilla").FindAction("CambiarFuerza").performed += ChangeForce;
        m_Input.FindActionMap("Ardilla").FindAction("Absorber").performed += Sniff;
        
    }

    private void OnEnable()
    {
        GuiActualObj.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        if (GuiActualObj == null) return;
        GuiActualObj.gameObject.SetActive(false);
    }

    private void Shoot(InputAction.CallbackContext coc)
    {
        GameObject go = squirrelController.getGameObject(selectedObj);
        if(selectedObj > squirrelController.listSize-1) selectedObj = squirrelController.listSize-1;
        if(selectedObj < 0) selectedObj = 0;
        if (go == null) return;
        go.transform.position = squirrelPosition.position;
        if(go.TryGetComponent<Rigidbody>(out Rigidbody rb))
        {
            rb.velocity = Vector3.zero;
            Vector3 dir = Vector3.zero;
            dir = (finalPos.position - squirrelPosition.position).normalized;
            go.transform.forward = dir;
            go.transform.rotation = Quaternion.LookRotation(dir,Vector3.up);
            go.SetActive(true);
            rb.AddForce(go.transform.forward * force, ForceMode.Impulse);
        }
    }

    private void FixedUpdate()
    {
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, force, layerMask))
        {
            finalPos.position = hit.point;
            posFinalFloor = Vector3.zero;
        }
        else
        {
            finalPos.position = ray.GetPoint(force);
            if (Physics.Raycast(finalPos.position, Vector3.down, out hit, 100, layerMask))
            {
                posFinalFloor = hit.point;
            }
        }
        Vector3 dir = (finalPos.position - squirrelPosition.position).normalized;
        squirrelPosition.rotation = Quaternion.LookRotation(dir, Vector3.up);
        if (squirrelController.HasGameObjects()) lineRenderer.material = hasBullets;
        else lineRenderer.material = noBullets;
        if (aiming)
        {
            lineRenderer.positionCount = nPoints;
            
            if(Physics.Raycast(finalPos.position, Vector3.down, out hit, 100, layerMask))
            {
                if(automaticTime) time = Vector3.Distance(finalPos.position, hit.point);
                Vector3 fin;
                if(posFinalFloor != Vector3.zero) fin = posFinalFloor;
                else fin = finalPos.position;
                Vector3[] posiocionesLineRenderer = CalculatePosition(squirrelPosition.position,squirrelPosition.forward * force/pMass,time);
                lineRenderer.SetPositions(posiocionesLineRenderer);
            }
        }
        else lineRenderer.positionCount = 0;
        GuiActualObj.showActualObject(squirrelController.showGameObject(selectedObj));
    }
    private void YesAim(InputAction.CallbackContext coc)
    {
        aiming = true;
    }
    private void NoAim(InputAction.CallbackContext coc)
    {
        aiming = false;
    }
    private void ChangeForce(InputAction.CallbackContext coc)
    {
        float a = coc.ReadValue<float>();
        if (aiming)
        {
            if(a > 0 && force < maxForce)force++;
            else if(a < 0 && force > minForce) force--;
        }
        else
        {
            if(a > 0 && selectedObj < squirrelController.listSize-1)selectedObj++;
            else if(a < 0 && selectedObj > 0) selectedObj--;
        }
        
    }
    
    private Vector3[] CalculatePosition(Vector3 init, Vector3 vel, float time)
    {
        Vector3[] posiciones = new Vector3[nPoints];
        posiciones[0] = init;
        for (int i = 1; i < nPoints; i++)
        {
            float tiempoActual = time * i;
            Vector3 posSinGravedad = vel * tiempoActual;
            Vector3 funcionGravedad = Vector3.up * -0.5f * Physics.gravity.y * tiempoActual * tiempoActual;
            Vector3 posicionActual = init + posSinGravedad - funcionGravedad;
            posiciones[i] = posicionActual;
        }
        return posiciones;
    }
    
    private void Sniff(InputAction.CallbackContext coc)
    {
        Physics.Raycast(camera.transform.position, camera.transform.forward, out RaycastHit hit, SnifDistance, layerPaint);
        if (hit.collider != null && hit.collider.gameObject.TryGetComponent<PaintedQuad>(out PaintedQuad qp))
        {
            if (qp.transform.localScale.x < 0.2f) return;
            GameObject balaPintura = Instantiate(BulletExample.bullet);
            balaPintura.transform.position = hit.point;
            hit.collider.TryGetComponent(out MeshRenderer mr);
            balaPintura.GetComponent<MeshRenderer>().material.color = mr.material.color;
            squirrelController.addGameObject(balaPintura);
            Destroy(qp.gameObject);
        }
    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Ardilla").FindAction("Disparar").performed -= Shoot;
        m_Input.FindActionMap("Ardilla").FindAction("Apuntar").started -= YesAim;
        m_Input.FindActionMap("Ardilla").FindAction("Apuntar").canceled -= NoAim;
        m_Input.FindActionMap("Ardilla").FindAction("CambiarFuerza").performed -= ChangeForce;
        m_Input.FindActionMap("Ardilla").FindAction("Absorber").performed -= Sniff;
    }
}
