using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OntriggerActivable : PSystem
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.TryGetComponent<CharacterInput>(out CharacterInput IDP)) Enable();
    }
}
