using Interficies;
using UnityEngine;

public class BeamCube : MonoBehaviour, IBounceableBeam
{
    private bool rebotando = false;


    public void BounceBeam(LineRenderer lineRenderer, Transform transform,float largoLaser)
    {
        lineRenderer.positionCount++;
        if(Physics.Raycast(transform.position,transform.forward,out RaycastHit hit,largoLaser))
        {
            lineRenderer.SetPosition(lineRenderer.positionCount-1,hit.point);
            if (hit.collider.TryGetComponent<IBounceableBeam>(out IBounceableBeam rl))
            {
                rl.BounceBeam(lineRenderer,hit.transform,largoLaser);
            }
        }
        else
        {
            lineRenderer.SetPosition(lineRenderer.positionCount-1,transform.position + transform.forward * largoLaser);
        }   
    }
}
