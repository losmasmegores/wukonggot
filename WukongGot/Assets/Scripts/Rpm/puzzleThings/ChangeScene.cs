using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    [SerializeField] private string escena;
    [SerializeField] private ScriptableChallengerMode scm;
    
    public void ChangeToScene()
    {
        if(GameManager.Instance != null) GameManager.Instance.GuardarNivel(escena);
        if(!scm.challengeMode) SceneManager.LoadScene(escena);
    }
    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ReturnToMenu()
    {
        if(scm.challengeMode)scm.challengeMode = false;
        SceneManager.LoadScene("MenuJogo");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
