using UnityEngine;

public class ColorPlate : PSystem
{
    private Material myMaterial;
    
    
    [Header("Working things")]
    [SerializeField] private Color colorWanted;
    [SerializeField] private float maxDesv;
    
    
    [Header("Decoracion")]
    [SerializeField] private Material GoodColor;
    [SerializeField] private Material BadColor;
    [SerializeField] private MeshRenderer showWantedColor;

    private void Awake()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
        showWantedColor.material.color = colorWanted;
    }

    private void OnCollisionEnter(Collision other)
    {
        other.gameObject.TryGetComponent(out MeshRenderer meshRenderer);
        Color receivedColor = meshRenderer.material.color;
        if (receivedColor == colorWanted)
        {
            GetComponent<MeshRenderer>().material = GoodColor;
            Enable();
        }
        else if(receivedColor.r+maxDesv > colorWanted.r && receivedColor.r - maxDesv < colorWanted.r &&
                receivedColor.g+maxDesv > colorWanted.g && receivedColor.g - maxDesv < colorWanted.g &&
                receivedColor.b+maxDesv > colorWanted.b && receivedColor.b - maxDesv < colorWanted.b)
        {
            GetComponent<MeshRenderer>().material = GoodColor;
            Enable();
        }
        else GetComponent<MeshRenderer>().material = BadColor;
    }

    private void OnCollisionExit(Collision other)
    {
        GetComponent<MeshRenderer>().material = myMaterial;
        Disable();
    }
}
