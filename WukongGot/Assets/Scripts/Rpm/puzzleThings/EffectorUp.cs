using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EffectorUp : MonoBehaviour
{
    [SerializeField] private float force;
    
    private List<Rigidbody> rbs = new List<Rigidbody>();

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Rigidbody>(out Rigidbody rigidb))
        {
            if (rbs.Contains(rigidb)) return;
            rbs.Add(rigidb);
        }
        else if (other.transform.parent.TryGetComponent<Rigidbody>(out Rigidbody rigidb2))
        {
            if (rbs.Contains(rigidb2)) return;
            rbs.Add(rigidb2);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Rigidbody>(out Rigidbody rigidb))
        {
            if (!rbs.Contains(rigidb)) return;
            rbs.Remove(rigidb);
        }
        else if (other.transform.parent.TryGetComponent<Rigidbody>(out Rigidbody rigidb2))
        {
            if (!rbs.Contains(rigidb2)) return;
            rbs.Remove(rigidb2);
        }
    }

    private void FixedUpdate()
    {
        foreach (Rigidbody rb in rbs)
        {
            if(rb != null)rb.AddForce(transform.up * force, ForceMode.Impulse);
        }
    }
}
