using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class GamePoolSquirrel : GamePool
{
    public GameObject GetElementSquirrel()
    {
        GameObject go = null;
        for (int i = 0; i < size; i++)
        {
            go = transform.GetChild(i).gameObject;
            if (!go.activeSelf && go.TryGetComponent<InteractableSquirrel>(out InteractableSquirrel squirrel))
            {
                if (!squirrel.GetHasSquirrel())
                {
                    activeMembers++;
                    return go;
                }
            }
        }
        return null;
    }
    public void ReturnElementSquirrel(GameObject go)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject selfgo = transform.GetChild(i).gameObject;
            if (selfgo == go)
            {
                activeMembers--;
                if(selfgo.TryGetComponent<InteractableSquirrel>(out InteractableSquirrel squirrel))
                {
                    squirrel.SetHasSquirrel(true);
                }
                go.SetActive(false);
                return;
            }
        }

        Assert.IsFalse(false);
    }
}
