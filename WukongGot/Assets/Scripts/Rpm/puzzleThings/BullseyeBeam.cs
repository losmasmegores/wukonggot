using UnityEngine;
using Interficies;

public class BullseyeBeam : PSystem, IBullseyeBeam
{
    private Material myMaterial;
   
    
    [Header("Funcionamiento")]
    [SerializeField] private Color colorWanted;
    [SerializeField] private float maxDesv;
   
    
    [Header("Decoracion")]
    [SerializeField] private Material colorGoodBuenoBueno;
    [SerializeField] private Material colorRedMaloMalo;
    [SerializeField] private MeshRenderer showWantedColor;
   
    private void Awake()
    {
        myMaterial = GetComponent<MeshRenderer>().material;
        showWantedColor.material.color = colorWanted;
    }
    public void BullseyeEnable(Color receivedColor)
    {
        if (receivedColor == colorWanted)
        {
            GetComponent<MeshRenderer>().material = colorGoodBuenoBueno;
            Enable();
        }
        else if (receivedColor.r + maxDesv > colorWanted.r && receivedColor.r - maxDesv < colorWanted.r &&
                receivedColor.g + maxDesv > colorWanted.g && receivedColor.g - maxDesv < colorWanted.g &&
                receivedColor.b + maxDesv > colorWanted.b && receivedColor.b - maxDesv < colorWanted.b)
        {
            GetComponent<MeshRenderer>().material = colorGoodBuenoBueno;
            Enable();
        }
        else
        {
            GetComponent<MeshRenderer>().material = colorRedMaloMalo;
            Disable();
        }

    }
    public void BullseyeDisable()
    {
        GetComponent<MeshRenderer>().material = colorRedMaloMalo;
        Disable(); 
        
    }
}