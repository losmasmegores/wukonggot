using Interficies;
using UnityEngine;
using UnityEngine.Serialization;

public class Bullseye : PSystem
{
    [SerializeField] private AudioSource bullseyeSound;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.TryGetComponent<IInteractableSquirrel>(out IInteractableSquirrel interactableSquirrel))
        {
            bullseyeSound.Play();
            Enable();
            this.GetComponent<MeshRenderer>().material.color = Color.green;
            
        }
    }
}
