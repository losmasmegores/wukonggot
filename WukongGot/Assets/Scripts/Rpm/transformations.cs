using System;

public enum transformations
{
    Base,Frog,Gorilla,Squirrel
}

public class Transformations
{
    public static String getTransformacionFromEnum(transformations t)
    {
        switch (t)
        {
            case transformations.Base:
                return "Base";
            case transformations.Frog:
                return "Rana";
            case transformations.Gorilla:
                return "Gorila";
            case transformations.Squirrel:
                return "Ardilla";
            default:
                return "Base";
        }
    }
    public static String getTransformationFromInt(int t)
    {
        switch (t)
        {
            case 0:
                return "Base";
            case 1:
                return "Rana";
            case 2:
                return "Gorila";
            case 3:
                return "Ardilla";
            default:
                return "Base";
        }
    }
    public static transformations getTransformacionEnumFromInt(int t)
    {
        switch (t)
        {
            case 0:
                return transformations.Base;
            case 1:
                return transformations.Frog;
            case 2:
                return transformations.Gorilla;
            case 3:
                return transformations.Squirrel;
            default:
                return transformations.Base;
        }
    }
    public static int getIntFromTransformacion(transformations t)
    {
        switch (t)
        {
            case transformations.Base:
                return 0;
            case transformations.Frog:
                return 1;
            case transformations.Gorilla:
                return 2;
            case transformations.Squirrel:
                return 3;
            default:
                return 0;
        }
    }
}
