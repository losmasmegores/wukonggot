
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Collections.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableChallengerMode", menuName = "Scriptable Objects/ScriptableChallengerMode")]
[Serializable]
public class ScriptableChallengerMode: ScriptableObject
{
    [SerializeField] public Player player;
    [SerializeField] public bool challengeMode;
    [SerializeField] public string playerjsonPath;
    [SerializeField] public string allplayersjsonPath;

    public void setChallengerMode(bool b)
    {
        challengeMode = b;
    }
}
