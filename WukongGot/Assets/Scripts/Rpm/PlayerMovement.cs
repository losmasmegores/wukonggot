using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.Timeline;
using UnityEngine.XR;

public enum PlayerStates
{
    Running,
    Jumping,
    Hovering,
    Static,
    Swinging,
    Walking,
    Climbing
}
public class PlayerMovement : MonoBehaviour
{
    private Rigidbody m_Rigidbody;
    //-------------------TRANSFORMATION-------------------
    private Transform m_Transform;
    private MovementVariables movementVar;
    //----------------------------------------------------
    
    //-------------------INPUT VARIABLES-------------------
    [SerializeField] private CharacterInput characterInput; 
    private InputActionAsset m_Input;
    private InputAction m_AccionMoverse;
    //---------------------------------------------------
    
    [Header("Player States")]
    [SerializeField] private PlayerStates state;
    
    [SerializeField]private bool running = false;
    [SerializeField]private bool jumping = false;
    [SerializeField]private bool hovering = false;
    [SerializeField]private bool swinging = false;

    [Header("Movement Variables")]
    [SerializeField] private float distanceFromFloor;
    [SerializeField] private Transform foot;
    [SerializeField] private LayerMask m_layermask;
    [SerializeField] private float multiplierVel;
    [SerializeField] private float hoveringVel;
    [SerializeField] private float floorDrag;
    [SerializeField] private float airDrag;

    [SerializeField] private float normalGravity;
    [SerializeField] private float hoveringGravity;
    private float gravity;

    private bool firsInteraction = false;


    private Vector3 up;
    //-----------------------------------------------------
    
    private void Awake()
    {
        gravity = normalGravity;
        up = transform.up;
        
        m_Transform = transform.GetChild(0);
        movementVar = m_Transform.GetComponent<MovementVariables>();
        
        m_Rigidbody = gameObject.GetComponent<Rigidbody>();
        
    }

    void Start()
    {
        m_Input = characterInput.m_Input;
       
        //-------------------------------INPUT ACTIONS--------------------------------
        //-------------------BASE-------------------
        setBaseActions(m_Input.FindActionMap("Base"));
        //------------------------------------------
        //-------------------RANA-------------------
        setBaseActions(m_Input.FindActionMap("Rana"));
        //------------------------------------------
        //------------------GORILA------------------
        setBaseActions(m_Input.FindActionMap("Gorila"));
        //------------------------------------------
        //-----------------ARDILLA------------------ 
        setBaseActions(m_Input.FindActionMap("Ardilla"));
        m_Input.FindActionMap("Ardilla").FindAction("Planear").performed += StartHovering;
        m_Input.FindActionMap("Ardilla").FindAction("Planear").canceled += StopHovering;
        //------------------------------------------
        
        //----------------------------------------------------------------------------
        
        
        TransformationVariables(transformations.Base);
    }

    private void setBaseActions(InputActionMap map)
    {
        map.FindAction("Saltar").performed += Jump;
        map.FindAction("Correr").performed += StartRunning;
        map.FindAction("Correr").canceled += StopRunning;   
    }
    private void disableBaseActions(InputActionMap map)
    {
        map.FindAction("Saltar").performed -= Jump;
        map.FindAction("Correr").performed -= StartRunning;
        map.FindAction("Correr").canceled -= StopRunning;   
    }
    
    public void TransformationVariables(transformations transformacion)
    {
        m_Input = characterInput.m_Input;
        String transformacionString = Transformations.getTransformacionFromEnum(transformacion);
        Debug.Log("Transformacion: "+transformacionString+"");
        Debug.Log("Transformacion: "+transformacionString+ " en Input: "+m_Input.name);
        InputActionMap m_InputMap = m_Input.FindActionMap(transformacionString);
        m_AccionMoverse = m_InputMap.FindAction("Movimiento");
        m_Transform = transform.GetChild(Transformations.getIntFromTransformacion(transformacion));
        movementVar = m_Transform.GetComponent<MovementVariables>();
    }
    
    private void Jump(InputAction.CallbackContext call)
    {
        RaycastHit ray;
        if (Physics.Raycast(foot.position, transform.up * -1f, out ray, distanceFromFloor,m_layermask))
        {
            Debug.DrawRay(foot.position, transform.up * -distanceFromFloor, Color.red, 3f);
            float velX = m_Rigidbody.velocity.x;
            float velZ = m_Rigidbody.velocity.z;
            if(m_Rigidbody.velocity.x > movementVar.AirVel) velX = movementVar.AirVel;
            if(m_Rigidbody.velocity.z > movementVar.AirVel) velZ = movementVar.AirVel;
            m_Rigidbody.velocity = new Vector3(velX,0,velZ);
            jumping = true;
            m_Rigidbody.drag = airDrag;
            ChangeState(PlayerStates.Jumping);
            m_Rigidbody.AddForce(transform.up * movementVar.JumpForce, ForceMode.Impulse);
        }
    }
    private void StartRunning(InputAction.CallbackContext call)
    {
        running = true;
        ChangeState(PlayerStates.Running);
    }
    private void StartRunning()
    {
        running = true;
        ChangeState(PlayerStates.Running);
    }
    private void StopRunning(InputAction.CallbackContext call)
    {
        running = false;
        ChangeState(PlayerStates.Walking);
    }
    private void StopRunning()
    {
        running = false;
        ChangeState(PlayerStates.Walking);
    }

    private void StartHovering(InputAction.CallbackContext call)
    {
        Debug.Log("Empezamos a planear");
        m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, hoveringGravity, m_Rigidbody.velocity.z);
        gravity = hoveringGravity;
        hovering = true;
        ChangeState(PlayerStates.Hovering);
    }
    private void StopHovering(InputAction.CallbackContext call)
    {
        Debug.Log("Dejamos de planear");
        gravity = normalGravity;
        hovering = false;
        ChangeState(PlayerStates.Static);
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
    }

    private void InitState(PlayerStates newState)
    {
        state = newState;
        switch (state)
        {
            case PlayerStates.Static:
                break;
            case PlayerStates.Walking:
                break;
            case PlayerStates.Running:
                break;
            case PlayerStates.Jumping:
                break;
            case PlayerStates.Hovering:
                break;
            case PlayerStates.Swinging:
                break;
        }
    }

    private void ExitState()
    {
        switch (state)
        {
            case PlayerStates.Static:
                break;
            case PlayerStates.Walking:
                break;
            case PlayerStates.Running:
                break;
            case PlayerStates.Jumping:
                break;
            case PlayerStates.Hovering:
                break;
            case PlayerStates.Swinging:
                break;
        }
    }

    private void UpdateState()
    {
        switch (state)
        {
            case PlayerStates.Static:
                break;
            case PlayerStates.Walking:
                break;
            case PlayerStates.Running:
                break;
            case PlayerStates.Jumping:
                break;
            case PlayerStates.Hovering:
                break;
            case PlayerStates.Swinging:
                break;
        }
    }

    public void ChangeState(PlayerStates newState)
    {
        if(state == newState) return;
        ExitState();
        InitState(newState);
    }

    private void CheckIfTouchFloor()
    {
        if (m_Rigidbody.velocity.y > 0) return;
        if(m_Rigidbody.velocity.y == 0 && jumping) return;
        if (state == PlayerStates.Walking && !jumping) return;
        if (state == PlayerStates.Running && !jumping) return;
        RaycastHit ray;
        if (Physics.Raycast(foot.position, transform.up * -1f, out ray, distanceFromFloor, m_layermask))
        {
            if (jumping) jumping = false;
            m_Rigidbody.drag = floorDrag;
            ChangeState(PlayerStates.Static);
        }
        else
        {
            if (Math.Abs(transform.localEulerAngles.x) > 70 || Math.Abs(transform.localEulerAngles.z) > 70)
                transform.up = up;
            m_Rigidbody.drag = airDrag;
        }
    }
    private void FixedUpdate()
    {
        
        CheckIfTouchFloor();
        CheckState();  
        float velocidad = movementVar.Vel;
        if(running)velocidad *= multiplierVel;
        if(jumping)velocidad = movementVar.AirVel;
        if(hovering)velocidad = hoveringVel;
        if(swinging)velocidad = movementVar.Vel;

        if(m_AccionMoverse.ReadValue<Vector2>().y < 0 && state == PlayerStates.Running)StopRunning();

        
        if(!m_Rigidbody.useGravity && state != PlayerStates.Climbing) m_Rigidbody.AddForce(m_Transform.up * -gravity, ForceMode.Force);
        MovementUpdate(velocidad);
    }

    private void MovementUpdate(float velocidad)
    {
        Vector3 direccion;
        if(state == PlayerStates.Climbing)direccion = m_Transform.up * m_AccionMoverse.ReadValue<Vector2>().y + m_Transform.right * m_AccionMoverse.ReadValue<Vector2>().x;
        else direccion = m_Transform.forward * m_AccionMoverse.ReadValue<Vector2>().y + m_Transform.right * m_AccionMoverse.ReadValue<Vector2>().x;
        m_Rigidbody.AddForce(direccion.normalized * velocidad, ForceMode.Force);
    }

    private void CheckState()
    {
        if (state == PlayerStates.Swinging) return;
        if (m_AccionMoverse.ReadValue<Vector2>() != Vector2.zero)
        {
            if (m_Rigidbody.velocity.y == 0)
            {
                if(running)ChangeState(PlayerStates.Running);
                else ChangeState(PlayerStates.Walking);
            }
        }else if((m_Rigidbody.velocity == Vector3.zero || m_AccionMoverse.ReadValue<Vector2>() == Vector2.zero) && !InTheAir())ChangeState(PlayerStates.Static);
    }
    
    public void Swinging(bool swing)
    {
        if(swing)ChangeState(PlayerStates.Swinging);
        else ChangeState(PlayerStates.Static);
        swinging = swing;
    }
    private bool InTheAir()
    {
        switch (state)
        {
            case PlayerStates.Jumping:
                return true;
            case PlayerStates.Hovering:
                return true;
            case PlayerStates.Swinging:
                return true;
            default: return false;
        }
    }

    private void OnDestroy()
    {
        //-------------------------------INPUT ACTIONS--------------------------------
        //-------------------BASE-------------------
        disableBaseActions(m_Input.FindActionMap("Base"));
        //------------------------------------------
        //-------------------RANA-------------------
        disableBaseActions(m_Input.FindActionMap("Rana"));
        //------------------------------------------
        //------------------GORILA------------------
        disableBaseActions(m_Input.FindActionMap("Gorila"));
        //------------------------------------------
        //-----------------ARDILLA------------------ 
        disableBaseActions(m_Input.FindActionMap("Ardilla"));
        m_Input.FindActionMap("Ardilla").FindAction("Planear").performed -= StartHovering;
        m_Input.FindActionMap("Ardilla").FindAction("Planear").canceled -= StopHovering;
        //------------------------------------------

    }
}

