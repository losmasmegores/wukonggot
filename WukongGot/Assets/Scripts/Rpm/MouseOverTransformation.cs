using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

public class MouseOverTransformation : MonoBehaviour
    , IPointerEnterHandler , IPointerExitHandler
{
    [SerializeField]private int numeroTransformacion;
    [FormerlySerializedAs("m_GestorTransformaciones")] [SerializeField] private TransformationsController transformationsController;

    [SerializeField] private Animator animaciones;
    [SerializeField] private CanvasManager _canvasManager;

    public void OnPointerEnter(PointerEventData eventData)
    {
        transformationsController.nextTransformation = numeroTransformacion;
        animaciones.Play("activar");
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        animaciones.Play("desactivar");
    }
}
