using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class LoadLevel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playText;

    private string savedLevelPath = "NivelActual.json";
    // Start is called before the first frame update
    private void Awake()
    {
        if(File.Exists(Application.persistentDataPath + "/" + savedLevelPath))
        {
            string json = File.ReadAllText(Application.persistentDataPath + "/" + savedLevelPath);
            LevelJson level = JsonUtility.FromJson<LevelJson>(json);
            Debug.Log(level.SceneName);
            Debug.Log(level.SceneName == "Tutorial Base");
            playText.text = level.SceneName == "Tutorial Base" && !level.phases[0].isComplete ? "Play" : "Continue";
        }
        else
        {
            playText.text = "Play";
        }
    }

    public void loadLevel()
    {
        if(File.Exists(Application.persistentDataPath + "/" + savedLevelPath))
        {
            string json = File.ReadAllText(Application.persistentDataPath + "/" + savedLevelPath);
            LevelJson level = JsonUtility.FromJson<LevelJson>(json);
            SceneManager.LoadScene(level.SceneName);
        }
        else
        {
            SceneManager.LoadScene("Tutorial Base");
        }
    }
}
