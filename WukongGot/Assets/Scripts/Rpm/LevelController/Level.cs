using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public string SceneName;
    public List<phase> phases;

    private void Awake()
    {
        SceneName = SceneManager.GetActiveScene().name;
    }

    public LevelJson setToJson()
    {
        LevelJson nj = new LevelJson(SceneName);
        foreach (phase fa in phases)
        {
            nj.phases.Add(new phaseJson(fa.isComplete, fa.ID));
        }
        return nj;
    }

    public void SavePhase(string faseID)
    {
        foreach (phase fa in phases)
        {
            if(fa.ID == faseID)
            {
                fa.Complete();
            }
        }
    }
}

[Serializable]
public class phase
{
    public bool isComplete = false;
    public string ID;
    public UnityEvent onCompletion;
    
    public void Complete()
    {
        isComplete = true;
    }
}
[Serializable]
public class LevelJson
{
    public string SceneName;
    public List<phaseJson> phases;

    public LevelJson(string sn)
    {
        this.SceneName = sn;
        this.phases = new List<phaseJson>();
    }
}
[Serializable]
public class phaseJson
{
    public bool isComplete = false;
    public string ID;

    public phaseJson(bool complete, string id)
    {
        this.isComplete = complete;
        this.ID = id;
    }
}
