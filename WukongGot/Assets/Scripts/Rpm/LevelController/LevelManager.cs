using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;


public class LevelManager : MonoBehaviour
{
    [SerializeField] private Level myLevel;
    [SerializeField] private ScriptableChallengerMode cM;
    private string ActualLevelPathName = "NivelActual.json";
    void Awake()
    {
        if (cM.challengeMode) return;
        if (File.Exists(Application.persistentDataPath + "/"+ ActualLevelPathName))
        {
            Debug.Log("--LOADING LEVEL--");
            string jsonplayers = File.ReadAllText(Application.persistentDataPath + "/" + ActualLevelPathName);
            LevelJson level = JsonUtility.FromJson<LevelJson>(jsonplayers);
            Debug.Log("--Scene : "+level.SceneName+" Compare to: "+myLevel.SceneName+"--");
            if (level.SceneName != myLevel.SceneName) return;
            Debug.Log("--LEVEL LOADED--");
            for (int i = 0; i < level.phases.Count; i++)
            {
                if (level.phases[i].isComplete)
                {
                    foreach (phase fa in myLevel.phases)
                    {
                        if (fa.ID == level.phases[i].ID)
                        {
                            fa.isComplete = true;
                            Debug.Log("--COMPLETING PHASE--");
                            fa.onCompletion.Invoke();   
                        }
                    }
                }
            }
        }   
    }

    public void saveNivel()
    {
        if(cM.challengeMode) return;
        File.WriteAllText(Application.persistentDataPath + "/" + ActualLevelPathName, JsonUtility.ToJson(myLevel.setToJson()));
    }
}
