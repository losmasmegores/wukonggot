using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerPosition : MonoBehaviour
{
    [SerializeField] private Transform player;

    public void setPlayerPos(Transform position)
    {
        player.position = position.position;
    }
}
