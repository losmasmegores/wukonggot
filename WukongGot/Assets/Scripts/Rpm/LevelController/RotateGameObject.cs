using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGameObject : MonoBehaviour
{
    [SerializeField] private Transform objectToRotate;

    public void Rotate(Transform rotation)
    {
        objectToRotate.localRotation = rotation.localRotation;
    }
}
