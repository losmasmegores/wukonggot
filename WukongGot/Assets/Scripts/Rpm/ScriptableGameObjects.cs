using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Collections.LowLevel.Unsafe;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableGameObjecs", menuName = "Scriptable Objects/ScriptableGameObjecs")]
[Serializable]
public class ScriptableGameObjects: ScriptableObject
{

    [SerializeField] private GameObject m_GameObjects;


    public GameObject getGameObject()
    {
        if (m_GameObjects != null)
        {
            GameObject go = m_GameObjects;
            m_GameObjects = null;
            return m_GameObjects;
        }
        else return null;
    }
    public void addGameObject(GameObject go)
    {
        m_GameObjects = go;
    }

}
