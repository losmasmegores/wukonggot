using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LogInRegisterController : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;
    [SerializeField] private ifIsLoggedChangeButtons iILC;
    
    [Header("Login")]
    [SerializeField] private TMP_InputField LogInName;
    [SerializeField] private TMP_InputField LogInPassword;
    
    [Header("Register")]
    [SerializeField] private TMP_InputField RegisterName;
    [SerializeField] private TMP_InputField RegisterPassword;
    public void LogIn()
    {
        string name = LogInName.text;
        string password = LogInPassword.text;

        if (playerController.LogIn(name, password))
        {
            LogInName.transform.parent.gameObject.SetActive(false);
            iILC.ChangeButtons();
        };
    }
    public void Register()
    {
        string name = RegisterName.text;
        string password = RegisterPassword.text;
        
        if (playerController.existPlayer(name))
        {
            Debug.Log("--PLAYER ALREADY EXISTS--");
            RegisterName.text = "";
            RegisterPassword.text = "";
        }
        else
        {
            
            playerController.createUser(name,password);
            RegisterName.transform.parent.gameObject.SetActive(false);
            LogInName.transform.parent.gameObject.SetActive(true);
            
        }
    }
    
    public void LogOut()
    {
        playerController.LogOut();
        iILC.ChangeButtons();
    }
}
