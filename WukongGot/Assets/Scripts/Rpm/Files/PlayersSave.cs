using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PlayersSave : MonoBehaviour
{
}

[Serializable]
public class Player
{
    public string name;
    public string password;
    public List<SceneScore> scores;
    
    public Player(string name, string password)
    {
        this.name = name;
        this.password = password;
        scores = new List<SceneScore>();
    }
    public bool LogIn(string password)
    {
        return this.password == password;
    }
    public float GetScore(string scene)
    {
        foreach (SceneScore sceneScore in scores)
        {
            if(sceneScore.scene == scene) return sceneScore.score;
        }
        return float.NaN;
    }
    public void SetScore(string scene, float score)
    {
        foreach (SceneScore sceneScore in scores)
        {
            if (sceneScore.scene == scene)
            {
                Debug.Log("--Actual score: "+sceneScore.score+" New score: "+score+"--");
                if (sceneScore.score <= score) return;
                sceneScore.score = score;
                return;
            };
        }
        scores.Add(new SceneScore(scene, score));
    }

    public void savePlayer(string soloPath,string allPath)
    {
        if (soloPath != null)
        {
            string json = JsonUtility.ToJson(this);
            File.WriteAllText(Application.persistentDataPath + "/" + soloPath, json);
        }
        
        if (File.Exists(Application.persistentDataPath + "/" + allPath))
        {
            string jsonplayers = File.ReadAllText(Application.persistentDataPath + "/" + allPath);
            PlayerList players = JsonUtility.FromJson<PlayerList>(jsonplayers);
            for (int i = 0; i < players.players.Count; i++)
            {
                if(players.players[i].name == this.name)
                {
                    players.players[i] = this;
                    File.WriteAllText(Application.persistentDataPath + "/" + allPath, JsonUtility.ToJson(players));
                    return;
                }
            }
        }
    }
}
[Serializable]
public class SceneScore
{
    public string scene;
    public float score;
    public SceneScore(string scene, float score)
    {
        this.scene = scene;
        this.score = score;
    }
}
