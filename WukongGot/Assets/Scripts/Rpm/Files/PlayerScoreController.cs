using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerScoreController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI playerName;
    [SerializeField] private TextMeshProUGUI playerScore;
    
    public void setPlayerScore(string name, string score)
    {
        playerName.text = name;
        playerScore.text = score;
    }
}
