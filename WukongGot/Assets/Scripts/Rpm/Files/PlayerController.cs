using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows;
using File = System.IO.File;
using Object = System.Object;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private string ActualPlayerPathName;
    [SerializeField] private string AllPlayersPathName;

    public string playerPath => ActualPlayerPathName;
    public string playersPath => AllPlayersPathName;
    
    public bool isLogedIn = false;

    public ScriptableChallengerMode scm;
    private void Awake()
    {
        if (File.Exists(Application.persistentDataPath + "/" + playerPath))
        {
            isLoged();
        }
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        scm.challengeMode = false;
    }

    public void isLoged()
    {
        isLogedIn = true;
    }

    public void isnotLoged()
    {
        isLogedIn = false;
    }
    public Player getActualPlayer()
    {
        string jsonplayer = File.ReadAllText(Application.persistentDataPath + "/" + playerPath);
        Player player = new Player("", "");
        JsonUtility.FromJsonOverwrite(jsonplayer, player);
        return player;
    }
    public PlayerList getPlayers()
    {
        string jsonplayers = File.ReadAllText(Application.persistentDataPath + "/" + playersPath);
        PlayerList players = JsonUtility.FromJson<PlayerList>(jsonplayers);
        return players;
    }
    
    public void LogOut()
    {
        if (File.Exists(Application.persistentDataPath + "/" + playerPath))
        {
            File.Delete(Application.persistentDataPath + "/" + playerPath);
            isnotLoged();
        }
    }

    public bool LogIn(String name, String password)
    {
        if(File.Exists(Application.persistentDataPath + "/" + playerPath))return false;
        PlayerList players = getPlayers();
        foreach (Player player in players.players)
        {
            if(player.name == name && player.LogIn(password))
            {
                File.WriteAllText(Application.persistentDataPath + "/" + playerPath, JsonUtility.ToJson(player));
                isLoged();
                return true;
            }
        }

        return false;
    }

    public void createUser(String name, String password)
    {
        Debug.Log("--CREATING USER--");
        PlayerList players;
        if (!File.Exists(Application.persistentDataPath + "/" + playersPath))
        {
            Debug.Log("--NO PLAYERS FILE FOUND--");
            players = new PlayerList();
            players.players.Add(new Player(name, password));
            
        }
        else
        {
            Debug.Log("--PLAYERS FILE FOUND--");
            players = getPlayers();
            players.players.Add(new Player(name, password));
        }
        Debug.Log("--PLAYER "+name+" CREATED--");
        File.WriteAllText(Application.persistentDataPath + "/" + playersPath, JsonUtility.ToJson(players));
        Debug.Log("--PLAYER "+name+" SAVED--");
    }

    public bool existPlayer(string name)
    {
        if (!File.Exists(Application.persistentDataPath + "/" + playersPath)) return false;
        PlayerList players = getPlayers();
        foreach (Player player in players.players)
        {
            if (player.name == name) return true;
        }

        return false;

    }

    public float getScoreFromPlayer()
    {
        if(!File.Exists(Application.persistentDataPath + "/" + playerPath))return float.NaN;
        Player player = getActualPlayer();
        return player.GetScore("scene");
    }
    public List<PlayerScore> getScoreFromPlayers(string scene)
    {
        if(!File.Exists(Application.persistentDataPath + "/" + playersPath))return null;
        PlayerList players = getPlayers();
        List<PlayerScore> leaderBoard = new List<PlayerScore>();
        foreach (Player player in players.players)
        {
            float score = player.GetScore(scene);
            if(float.IsNaN(score))continue;
            leaderBoard.Add(new PlayerScore(player.name, score));
        }
        leaderBoard.Sort((a, b) => a.score.CompareTo(b.score));
        return leaderBoard;
    }

    #region Debug

    public void showAllPlayers()
    {
        if (!File.Exists(Application.persistentDataPath + "/" + playersPath))
        {
            Debug.Log("--NO PLAYERS FILE FOUND--");
            return;
        }
        Debug.Log("--LOADING PLAYERS--");
        PlayerList players = getPlayers();
        foreach (Player player in players.players)
        {
            Debug.Log("--PLAYER FOUND -> " + player.name + " --");
        }
        Debug.Log("--ALL PLAYERS LOADED--");
        
    }
    
    #endregion
}

public class PlayerScore
{
    public string name;
    public float score;
    
    public PlayerScore(string name, float score)
    {
        this.name = name;
        this.score = score;
    }
}

[Serializable]
public class PlayerList
{
    public List<Player> players;
    public PlayerList()
    {
        players = new List<Player>();
    }
}
