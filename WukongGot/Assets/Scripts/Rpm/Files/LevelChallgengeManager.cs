using System.Collections;
using System.Collections.Generic;
using System.Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChallgengeManager : MonoBehaviour
{
    [SerializeField] private ScriptableChallengerMode scm;

    [SerializeField] private float time;
    [SerializeField] private TextMeshProUGUI timerText;
    private bool finish = false;
    void Start()
    {
        if (scm.challengeMode)
        {
            timerText.gameObject.SetActive(true);
            StartCoroutine(empiezaCarrera());
        }
    }
    public void save()
    {
        Debug.Log("--SAVING--");
        scm.player.SetScore(SceneManager.GetActiveScene().name,time);
        scm.player.savePlayer(scm.playerjsonPath,scm.allplayersjsonPath);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene("MenuJogo");
    }

    public IEnumerator empiezaCarrera()
    {
        time = 0;
        while (!finish)
        {
            time += 1f;
            timerText.text = SetTime(time);
            yield return new WaitForSeconds(0.01f);
        }

        save();
        scm.challengeMode = false;
    }

    public string SetTime(float time)
    {
        int seconds = (int)time / 100;
        int miliseconds = (int)time % 100;
        return string.Format("{0:00}:{1:00}",seconds,miliseconds);
    }
    public void isFinish()
    {
        finish = true;
    }
}
