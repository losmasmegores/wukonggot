using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ifIsLoggedChangeButtons : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;
    [SerializeField] private RectTransform LogInButton;
    [SerializeField] private RectTransform RegisterButton;
    [SerializeField] private RectTransform LogOutButton;
    [SerializeField] private TextMeshProUGUI User;
    private void OnEnable()
    {
        ChangeButtons();
    }
    public void ChangeButtons()
    {
        if(playerController.isLogedIn)
        {
            LogInButton.gameObject.SetActive(false);
            RegisterButton.gameObject.SetActive(false);
            LogOutButton.gameObject.SetActive(true);
            User.gameObject.SetActive(true);
            Player player = playerController.getActualPlayer();
            User.text = player.name;
        }
        else
        {
            LogInButton.gameObject.SetActive(true);
            RegisterButton.gameObject.SetActive(true);
            LogOutButton.gameObject.SetActive(false);
            User.gameObject.SetActive(false);
        }
    }
}
