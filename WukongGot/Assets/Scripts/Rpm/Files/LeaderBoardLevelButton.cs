using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderBoardLevelButton : MonoBehaviour
{
    [SerializeField] private LeaderBoardController leaderBoardController;
    [SerializeField] private string levelName;
    [SerializeField] private string SceneName;
    [SerializeField] private TextMeshProUGUI text;
    public void ChangeLeaderBoard()
    {
        leaderBoardController.ChangeLeaderBoard(levelName,SceneName);
    }
    public void setNames(string lN, string sN)
    {
        levelName = lN;
        SceneName = sN;
    }

    public void setNumber(int i)
    {
        text.text = i.ToString();
    }
}
