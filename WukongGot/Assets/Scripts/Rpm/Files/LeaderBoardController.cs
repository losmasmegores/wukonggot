using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderBoardController : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;
    [SerializeField] private TextMeshProUGUI ActualLevelText;
    [Header("LeaderBoard")]
    [SerializeField] private RectTransform LeaderBoard;
    [SerializeField] private GameObject playerScorePrefab;
    [SerializeField] private TextMeshProUGUI textIfNoPlayers;
    
    [Header("LeaderBoardLevels")]
    [SerializeField] private RectTransform LeaderBoardLevels;
    [SerializeField] private GameObject LeaderBoardLevelPrefab;

    [Header("First Board To Show")]
    [SerializeField] private string FirstBoardToShow;
    [SerializeField] private string FirstSceneToShow;
    // [SerializeField] private 
    private void OnEnable()
    {
        ChangeLeaderBoard(FirstBoardToShow,FirstSceneToShow);
    }

    private void OnDisable()
    {
        clearLeaderBoard();
    }

    public void createLevel(int num,string levelName, string sceneName)
    {
        GameObject go = Instantiate(LeaderBoardLevelPrefab);
        go.transform.parent = LeaderBoardLevels;
        LeaderBoardLevelButton lb = go.GetComponent<LeaderBoardLevelButton>();
        lb.setNames(levelName,sceneName);
        lb.setNumber(num);
        go.SetActive(true);
    }
    public void ChangeLeaderBoard(string levelName,string sceneName)
    {
        clearLeaderBoard();
        ActualLevelText.text = levelName;
        List<PlayerScore> playerScores = playerController.getScoreFromPlayers(sceneName);
        if(playerScores.Count == 0)
        {
            textIfNoPlayers.text = "No Players in this Level";
            textIfNoPlayers.gameObject.SetActive(true);
            LeaderBoard.sizeDelta = new Vector2(LeaderBoard.sizeDelta.x, 3);
            LeaderBoard.position = new Vector3(LeaderBoard.position.x, LeaderBoard.position.y - LeaderBoard.sizeDelta.y / 2, LeaderBoard.position.z);
        }
        else
        {
            textIfNoPlayers.gameObject.SetActive(false);
            GameObject go = null;
            int num = 0;
            foreach (PlayerScore player in playerScores)
            {
                go = Instantiate(playerScorePrefab);
                go.transform.parent = LeaderBoard;
                int seconds = (int)player.score / 100;
                int miliseconds = (int)player.score % 100;
                go.GetComponent<PlayerScoreController>().setPlayerScore(player.name, string.Format("{0:00}:{1:00}",seconds,miliseconds));
                go.SetActive(true);
                num++;
            };
            RectTransform rtlastGo = go.GetComponent<RectTransform>();
            LeaderBoard.sizeDelta = new Vector2(LeaderBoard.sizeDelta.x, rtlastGo.sizeDelta.y * (num-1));
            LeaderBoard.position = new Vector3(LeaderBoard.position.x, LeaderBoard.position.y - LeaderBoard.sizeDelta.y / 2, LeaderBoard.position.z);
        }
    }
    public void clearLeaderBoard()
    {
        foreach (Transform child in LeaderBoard)
        {
            Destroy(child.gameObject);
        }
    }
}
